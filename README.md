# Bareblocks engine

This is my hobby project for creating a Minecraft-like voxel game engine from scratch using openGL. Key features are easy moddability and multiplayer support.

The project is remotely base on [this tutorial](https://lwjglgamedev.gitbooks.io/3d-game-development-with-lwjgl/content/), but I have heavily adapted the code to fit my purposes. However, some textures and 3d-models are temporarily direct copies from the tutorial.

The current version of this project is now fully functional. However, it contains a lot of deprecated code and runs at very low fps. Heavy optimizations are on the way.