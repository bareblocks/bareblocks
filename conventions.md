# Conventions used in code

## Code style

- One indentation = 4 spaces
- All block starting curly brackets on the same line as block definition (class, method, loop, if statement, etc.)
- One space before left parenthesis and curly bracket in loops, if statements, switch statements and try-catch statements
- Multiline class/method/etc. definitions and calls can be used when the text would otherwise exceed 100 characters
- Else-statement on the same line as the closing curly bracket of if-statement
- Else if -statement on one line
- Catch- and finally-statements on the same line as the closing curly bracket of the previous statement
- Wrap switch cases in curly brackets if multiline

## Directions

- Positive y-axis is up, negative down
- Positive x-axis is north, negative south
- Positive z-axis is east, negative west