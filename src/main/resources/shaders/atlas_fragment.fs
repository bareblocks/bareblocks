#version 330

in vec2 outTexCoord;

layout(location = 0) out vec4 fragColor;

uniform sampler2D textureSampler;

void main()
{
    fragColor = texture(textureSampler, outTexCoord);
}