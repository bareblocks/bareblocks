#version 430

layout (location=0) in vec3 position;
layout (location=1) in vec2 texCoord;
layout (location=2) in vec3 vertexNormal;

out vec2 outTexCoord;
out vec3 mvVertexNormal;
out vec3 mvVertexPos;
out vec4 faceColour;
out float gl_ClipDistance[1];

uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;

uniform vec2 atlasTextureSize;
uniform isampler3D xSampler;
uniform isampler3D ySampler;
uniform isampler3D zSampler;

void main()
{
    // Calculate position of this vertex
    vec4 mvPos = modelViewMatrix * vec4(position, 1.0);
    gl_Position = projectionMatrix * mvPos;
    mvVertexNormal = normalize(modelViewMatrix * vec4(vertexNormal, 0.0)).xyz;
    mvVertexPos = mvPos.xyz;

    // Calculate vertex visibility and texture id
    float x;
    float y;
    float z;
    int textureId;
    if(vertexNormal.x != 0)
    {
        x = position.x;
        if(vertexNormal.x > 0)
        {
            y = position.y - texCoord.y;
            z = position.z + texCoord.x - 1;
        }
        else
        {
            y = position.y - texCoord.y;
            z = position.z - texCoord.x;
        }
        textureId = texture(xSampler, vec3((x+0.5)/16, (y+0.5)/16, (z+0.5)/16)).r;
        gl_ClipDistance[0] = textureId * vertexNormal.x - 0.5;
    }
    else if(vertexNormal.y != 0)
    {
        y = position.y;
        if(vertexNormal.y > 0)
        {
            x = position.x - texCoord.y;
            z = position.z - texCoord.x;
        }
        else
        {
            x = position.x + texCoord.y - 1;
            z = position.z - texCoord.x;
        }
        textureId = texture(ySampler, vec3((x+0.5)/16, (y+0.5)/16, (z+0.5)/16)).r;
        gl_ClipDistance[0] = textureId * vertexNormal.y - 0.5;
    }
    else if(vertexNormal.z != 0)
    {
        z = position.z;
        if(vertexNormal.z > 0)
        {
            x = position.x - texCoord.x;
            y = position.y - texCoord.y;
        }
        else
        {
            x = position.x + texCoord.x - 1;
            y = position.y - texCoord.y;
        }
        textureId = texture(zSampler, vec3((x+0.5)/16, (y+0.5)/16, (z+0.5)/16)).r;
        gl_ClipDistance[0] = textureId * vertexNormal.z - 0.5;
    }
    else
    {
        // This never happens in a chunk mesh
        textureId = 0;
        gl_ClipDistance[0] = -1;
    }
    //faceColour = vec4(1, 0, textureId/4, 1);
    faceColour = texture(ySampler, vec3(0.5/16, 6.5/16, 0.5/16)).r == 2 ? vec4(0, 1, 0, 1) : vec4(1, 0, 0, 1);
    //if(textureId > 256) faceColour = vec4(1, 1, 1, 1); // white
    //else if(textureId > 64) faceColour = vec4(1, 1, 0, 1); // yellow
    //else if(textureId > 16) faceColour = vec4(1, 0, 1, 1); // purple
    //else if(textureId > 4) faceColour = vec4(0, 1, 1, 1); // cyan
    //else if(textureId > 0) faceColour = vec4(0, 1, 0.5, 1); // light green
    //else if(textureId < -256) faceColour = vec4(1, 0, 0.5, 1); // pink
    //else if(textureId < -64) faceColour = vec4(1, 0, 0, 1); // red
    //else if(textureId < -16) faceColour = vec4(0, 1, 0, 1); // green
    //else if(textureId < -4) faceColour = vec4(0, 0, 1, 1); // blue
    //else if(textureId < 0) faceColour = vec4(0.5, 0, 0.5, 1); // dark purple
    //else faceColour = vec4(0.5, 0.5, 0.5, 1); // gray
    // Calculate texture coordinate based on the texture id
    outTexCoord = vec2(texCoord.x + atlasTextureSize.x * mod(textureId, 8), texCoord.y + atlasTextureSize.y * (textureId / 8));
}