package me.anttono.bareblocks.templates;

import me.anttono.bareblocks.api.IntGenerator2D;

public class WorldLayerTemplate {
    private final String name;
    private final IntGenerator2D topHeightGenerator;
    private final int defaultBlock;
    private final int topBlock;

    public WorldLayerTemplate(String name, IntGenerator2D topHeightGenerator, int defaultBlock, int topBlock) {
        this.name = name;
        this.topHeightGenerator = topHeightGenerator;
        this.defaultBlock = defaultBlock;
        this.topBlock = topBlock;
    }

    /**
     * Initialize the height generator of this layer for a world.
     * @param seeds the sub-seeds for the world
     */
    public void init(int[] seeds) {
        this.topHeightGenerator.init(seeds);
    }

    public int generateHeight(int x, int z) {
        return topHeightGenerator.generate(x, z);
    }

    public int getTopBlock() {
        return topBlock;
    }

    public int getDefaultBlock() {
        return defaultBlock;
    }
}
