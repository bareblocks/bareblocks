package me.anttono.bareblocks.templates;

public class ItemTemplate {
    // The mod this item came from
    private final Mod mod;

    // The internal and unique name for this item. Used in pretty much everything
    private final String name;

    // How many items can fit in one inventory slot
    private final int stackSize;

    public ItemTemplate(Mod mod, String name, String texturePath, int stackSize) {
        this.mod = mod;
        this.name = name;
        this.stackSize = stackSize;
    }
}
