package me.anttono.bareblocks.templates;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

public class WorldGeneratorTemplate {
    private final String name;
    private final List<WorldLayerTemplate> layers;

    public WorldGeneratorTemplate(String name, List<WorldLayerTemplate> layers) {
        this.name = name;
        this.layers = layers;
    }

    /**
     * Initialize this world generator for a world. All WorldLayerTemplates will be initialized
     * @param seeds array of sub-seeds for the world
     */
    public void init(int[] seeds) {
        for (WorldLayerTemplate layer : layers) {
            layer.init(seeds);
        }
    }

    public int getLayerCount() {
        return layers.size();
    }

    public List<WorldLayerTemplate> getLayers() {
        return layers;
    }
}
