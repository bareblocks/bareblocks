package me.anttono.bareblocks.templates;

import me.anttono.bareblocks.modloader.data.DataSource;
import me.anttono.bareblocks.modloader.exceptions.ModLoaderException;
import me.anttono.bareblocks.modloader.data.TextureData;
import me.anttono.bareblocks.templates.textures.BlockTexture;
import me.anttono.bareblocks.utils.ModUtils;

import java.util.Map;

public class BlockTemplate implements Comparable<BlockTemplate> {
    private final String name;
    private final TextureData textureData;
    private BlockTexture texture;

    public BlockTemplate(String name, TextureData textureData) {
        this.name = name;
        this.textureData = textureData;
        this.texture = null;
    }

    public BlockTemplate(String name, BlockTexture texture) {
        this.name = name;
        this.textureData = null;
        this.texture = texture;
    }

    @Override
    public int compareTo(BlockTemplate other) {
        return name.compareTo(other.getName());
    }

    public void constructTexture(Mod[] mods, Map<String, Integer> textureIds) throws ModLoaderException {
        if (textureData == null) return;
        // Verify fields of textureData
        ModUtils.requireWordFieldExistence(textureData.getCommon(), "common", textureData);
        ModUtils.requireWordField(textureData.getTop(), "top", textureData);
        ModUtils.requireWordField(textureData.getBottom(), "bottom", textureData);
        ModUtils.requireWordField(textureData.getNorth(), "north", textureData);
        ModUtils.requireWordField(textureData.getEast(), "east", textureData);
        ModUtils.requireWordField(textureData.getSouth(), "south", textureData);
        ModUtils.requireWordField(textureData.getWest(), "west", textureData);
        // Construct BlockTexture out of textureData
        Mod sourceMod = textureData.getSourceMod();
        int defaultTextureId = textureIds.get(ModUtils.globalTexturePath(mods, sourceMod, textureData.getCommon(), textureData));
        int topTextureId = getTextureId(mods, sourceMod, textureIds, textureData.getTop(), defaultTextureId, textureData);
        int bottomTextureId = getTextureId(mods, sourceMod, textureIds, textureData.getBottom(), defaultTextureId, textureData);
        int northTextureId = getTextureId(mods, sourceMod, textureIds, textureData.getNorth(), defaultTextureId, textureData);
        int eastTextureId = getTextureId(mods, sourceMod, textureIds, textureData.getEast(), defaultTextureId, textureData);
        int southTextureId = getTextureId(mods, sourceMod, textureIds, textureData.getSouth(), defaultTextureId, textureData);
        int westTextureId = getTextureId(mods, sourceMod, textureIds, textureData.getWest(), defaultTextureId, textureData);
        this.texture = new BlockTexture(topTextureId, bottomTextureId, northTextureId, eastTextureId, southTextureId, westTextureId);
    }

    private int getTextureId(Mod[] mods, Mod sourceMod, Map<String, Integer> textureIds, String textureName, int defaultTextureId, DataSource source) throws ModLoaderException {
        return textureName != null
                ? textureIds.get(ModUtils.globalTexturePath(mods, sourceMod, textureName, source))
                : defaultTextureId;
    }

    public String getName() {
        return name;
    }

    public BlockTexture getTexture() {
        return texture;
    }
}
