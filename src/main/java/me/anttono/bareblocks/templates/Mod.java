package me.anttono.bareblocks.templates;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Mod implements Comparable<Mod> {
    private String path;

    @JsonProperty
    private String mod;       // Acts as the internal name of the mod
    @JsonProperty
    private String name;
    @JsonProperty
    private String version;
    @JsonProperty
    private String description;
    @JsonProperty
    private String scriptFolder;
    @JsonProperty
    private String textureFolder;
    @JsonProperty
    private String jarFile;

    // Default constructor required for YAML parsing
    public Mod() {}

    public Mod(String mod, String name, String version, String description, String scriptFolder, String textureFolder, String jarFile) {
        this.mod = mod;
        this.name = name;
        this.version = version;
        this.description = description;
        this.scriptFolder = scriptFolder;
        this.textureFolder = textureFolder;
        this.jarFile = jarFile;
    }

    @Override
    public int compareTo(Mod other) {
        return mod.compareTo(other.getMod());
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMod() {
        return mod;
    }

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    public String getDescription() {
        return description;
    }

    public String getScriptFolder() {
        return scriptFolder;
    }

    public String getScriptFolderPath() {
        return path + "\\" + scriptFolder;
    }

    public String getTextureFolder() {
        return textureFolder;
    }

    public String getTextureFolderPath() {
        return path + "\\" + textureFolder;
    }

    public String getJarFile() {
        return jarFile;
    }

    public String getJarFilePath() {
        return path + "\\" + jarFile;
    }
}
