package me.anttono.bareblocks.templates.textures;

public class BlockTexture {
    private int topTextureId;
    private int bottomTextureId;
    private int northTextureId;
    private int eastTextureId;
    private int southTextureId;
    private int westTextureId;

    /**
     * A constructor.
     * @param topTextureId    id to the texture that will be used as the top face of the block.
     * @param bottomTextureId id to the texture that will be used as the bottom face of the block.
     * @param northTextureId  id to the texture that will be used as the north face of the block.
     * @param eastTextureId   id to the texture that will be used as the east face of the block.
     * @param southTextureId  id to the texture that will be used as the south face of the block.
     * @param westTextureId   id to the texture that will be used as the west face of the block.
     */
    public BlockTexture(int topTextureId, int bottomTextureId, int northTextureId, int eastTextureId, int southTextureId, int westTextureId) {
        this.topTextureId = topTextureId;
        this.bottomTextureId = bottomTextureId;
        this.northTextureId = northTextureId;
        this.eastTextureId = eastTextureId;
        this.southTextureId = southTextureId;
        this.westTextureId = westTextureId;
    }

    public int getTopTextureId() {
        return topTextureId;
    }

    public int getBottomTextureId() {
        return bottomTextureId;
    }

    public int getNorthTextureId() {
        return northTextureId;
    }

    public int getEastTextureId() {
        return eastTextureId;
    }

    public int getSouthTextureId() {
        return southTextureId;
    }

    public int getWestTextureId() {
        return westTextureId;
    }
}
