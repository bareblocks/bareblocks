package me.anttono.bareblocks;

import me.anttono.bareblocks.client.Client;
import me.anttono.bareblocks.client.io.Window;

public class Main {
    public static void main(String[] args) {
        // Start the client
        Window window = new Window("Bareblocks");
        Client client = new Client(window);
        client.start();
    }
}
