package me.anttono.bareblocks.client.world;

import me.anttono.bareblocks.api.types.BlockPos;
import me.anttono.bareblocks.api.types.ChunkPos;
import me.anttono.bareblocks.api.types.Direction;
import me.anttono.bareblocks.api.types.WorldPos;
import me.anttono.bareblocks.utils.threading.EventRequestChat;
import me.anttono.bareblocks.utils.threading.requests.ChunkLoadRequest;
import org.joml.Vector3f;
import org.joml.Vector3i;

import java.util.Optional;
import java.util.function.DoubleBinaryOperator;

public class World {
    public static final int CHUNK_COORD_LIMIT = 15;
    public static final float MAX_INTERACT_DISTANCE = 10f;

    // Chat to send requests to server
    private final EventRequestChat chat;

    // World render data
    private int renderDistance;
    private RenderChunk[][][] chunks = new RenderChunk[0][0][0];
    private ChunkPos cameraChunkPos = new ChunkPos();

    public World(EventRequestChat chat, int renderDistance) {
        this.chat = chat;

        this.setRenderDistance(renderDistance);
    }

    public void setRenderDistance(int renderDistance) {
        this.renderDistance = renderDistance;

        // Cache the previously loaded chunks
        RenderChunk[][][] loadedChunks = chunks;
        // Create a new array for the chunks and save the already loaded chunks to it
        chunks = new RenderChunk[renderDistance * 2][renderDistance * 2][renderDistance * 2];
        for (RenderChunk[][] arr2d : loadedChunks)
            for (RenderChunk[] arr : arr2d)
                for (RenderChunk chunk : arr) {
                    if (chunk == null) continue;
                    GridPos gridPos = new GridPos(chunk.getPos());
                    setGridChunk(gridPos, chunk);
                }
        // Load the missing chunks after updating the render distance
        loadChunks();
    }

    public void setCameraWorldPos(WorldPos pos, boolean forceGenerate) {
        ChunkPos chunk = pos.toChunkPos();

        if (!cameraChunkPos.equals(chunk) || forceGenerate) {
            System.out.println("World: player crossed chunk border.");

            cameraChunkPos = chunk;

            loadChunks();
        }
    }

    /**
     * Requests the missing chunks around the player from the server.
     */
    private void loadChunks() {
        for (int i = -renderDistance + 1; i < renderDistance; i++) {
            for (int j = -renderDistance + 1; j < renderDistance; j++) {
                for (int k = -renderDistance + 1; k < renderDistance; k++) {
                    ChunkPos chunkPos = new ChunkPos(cameraChunkPos.x + i, cameraChunkPos.y + j, cameraChunkPos.z + k);
                    GridPos gridPos = new GridPos(chunkPos);
                    if (!isChunkLoaded(chunkPos)) {
                        chat.addRequest(new ChunkLoadRequest(0, chunkPos));
                    }
                }
            }
        }
    }

    /**
     * Get the RenderChunk at the given position if it is loaded. Otherwise, returns null.
     * @param chunkPos world position of the chunk to find
     * @return Optional of the RenderChunk at the given position if loaded, Optional.empty() otherwise.
     */
    public Optional<RenderChunk> getChunk(ChunkPos chunkPos) {
        GridPos gridPos = new GridPos(chunkPos);
        if (isChunkLoaded(chunkPos)) {
            return Optional.of(getGridChunk(gridPos));
        }
        return Optional.empty();
    }

    /**
     * Set the RenderChunk at the given position of this world to be the given chunk.
     * May clean up and overwrite an existing chunk if it is more than twice the render distance away from the new chunk.
     * @param chunkPos   a chunk position
     * @param chunk the chunk to set
     */
    public void setChunk(ChunkPos chunkPos, RenderChunk chunk) {
        // Set the new chunk
        GridPos gridPos = new GridPos(chunkPos);
        if (getGridChunk(gridPos) != null) getGridChunk(gridPos).cleanup();
        setGridChunk(gridPos, chunk);

        // Regenerate meshes for existing chunks (north, up and east)
        getChunk(new ChunkPos(chunkPos.x + 1, chunkPos.y, chunkPos.z)).ifPresent(RenderChunk::regenerateMesh);
        getChunk(new ChunkPos(chunkPos.x, chunkPos.y + 1, chunkPos.z)).ifPresent(RenderChunk::regenerateMesh);
        getChunk(new ChunkPos(chunkPos.x, chunkPos.y, chunkPos.z + 1)).ifPresent(RenderChunk::regenerateMesh);
    }

    public RenderChunk[][][] getChunks() {
        return chunks;
    }

    /**
     * Gets the id of the block at the given coordinates in this world.
     * @param pos coordinates of the block
     * @return the id of the block at the given coordinates. If the given position is currently not loaded, -1 will be returned
     */
    public int getBlockAt(BlockPos pos) {
        ChunkPos chunkPos = pos.toChunkPos();
        GridPos gridPos = new GridPos(chunkPos);
        ChunkPos.InternalBlockPos blockPos = chunkPos.getInternalBlockPos(pos);

        if (!isChunkLoaded(chunkPos))
            return -1;
        else return getGridChunk(gridPos).getBlockAt(blockPos);
    }

    public void setBlock(BlockPos pos, int blockType) {
        ChunkPos chunkPos = pos.toChunkPos();
        GridPos gridPos = new GridPos(chunkPos);
        ChunkPos.InternalBlockPos blockPos = chunkPos.getInternalBlockPos(pos);

        if (!isChunkLoaded(chunkPos))
            return;

        // Set the block type at the blockPos
        getGridChunk(gridPos).setBlock(blockPos, blockType);
        // Regenerate meshes for existing chunks (north, up and east) if necessary
        if (blockPos.x == CHUNK_COORD_LIMIT)
            getChunk(new ChunkPos(chunkPos.x + 1, chunkPos.y, chunkPos.z)).ifPresent(RenderChunk::regenerateMesh);
        if (blockPos.y == CHUNK_COORD_LIMIT)
            getChunk(new ChunkPos(chunkPos.x, chunkPos.y + 1, chunkPos.z)).ifPresent(RenderChunk::regenerateMesh);
        if (blockPos.z == CHUNK_COORD_LIMIT)
            getChunk(new ChunkPos(chunkPos.x, chunkPos.y, chunkPos.z + 1)).ifPresent(RenderChunk::regenerateMesh);
    }

    /**
     * Raytrace the world grid to find information about the nearest block in the given direction from the given position.
     * @param position    to start raytracing from.
     * @param direction   to raytrace toward.
     * @param maxDistance the maximum distance in blocks to raytrace.
     * @return a RaytraceResult containing the position of the found block if any were found and the direction of the
     * face of the block that the raytrace hit.
     */
    private Optional<RaytraceResult> raytrace(WorldPos position, Vector3f direction, float maxDistance) {
        Vector3f unitDirection = direction.normalize();

        float bestDistance = maxDistance;
        BlockPos bestPos = null;
        Direction bestDirection = null;

        // Way to figure out the distance from the difference in one coordinate
        // and the direction component along the same coordinate axis
        DoubleBinaryOperator distance = (double difference, double scale) -> {
            if (scale == 0) {
                return 1e9;
            } else {
                return Math.abs(difference / scale);
            }
        };

        // X-axis
        int x = position.toBlockPos().x;
        if (unitDirection.x > 0) x++;
        float currentDistance = (float) distance.applyAsDouble(position.x - x, unitDirection.x);
        while (currentDistance < bestDistance) {
            BlockPos foundPos;
            Direction foundDirection;
            if (unitDirection.x < 0) {
                foundPos = new WorldPos(x - 1, position.y + currentDistance * unitDirection.y, position.z + currentDistance * unitDirection.z).toBlockPos();
                foundDirection = Direction.NORTH;
                x--;
            } else {
                foundPos = new WorldPos(x, position.y + currentDistance * unitDirection.y, position.z + currentDistance * unitDirection.z).toBlockPos();
                foundDirection = Direction.SOUTH;
                x++;
            }

            if (getBlockAt(foundPos) > 0) {
                bestDistance = currentDistance;
                bestDirection = foundDirection;
                bestPos = foundPos;
            }

            currentDistance = (float) distance.applyAsDouble(position.x - x, unitDirection.x);
        }
        // Y-axis
        int y = position.toBlockPos().y;
        if (unitDirection.y > 0) y++;
        currentDistance = (float) distance.applyAsDouble(position.y - y, unitDirection.y);
        while (currentDistance < bestDistance) {
            BlockPos foundPos;
            Direction foundDirection;
            if (unitDirection.y < 0) {
                foundPos = new WorldPos(position.x + currentDistance * unitDirection.x, y - 1, position.z + currentDistance * unitDirection.z).toBlockPos();
                foundDirection = Direction.UP;
                y--;
            } else {
                foundPos = new WorldPos(position.x + currentDistance * unitDirection.x, y, position.z + currentDistance * unitDirection.z).toBlockPos();
                foundDirection = Direction.DOWN;
                y++;
            }

            if (getBlockAt(foundPos) > 0) {
                bestDistance = currentDistance;
                bestDirection = foundDirection;
                bestPos = foundPos;
            }

            currentDistance = (float) distance.applyAsDouble(position.y - y, unitDirection.y);
        }
        // Z-axis
        int z = position.toBlockPos().z;
        if (unitDirection.z > 0) z++;
        currentDistance = (float) distance.applyAsDouble(position.z - z, unitDirection.z);
        while (currentDistance < bestDistance) {
            BlockPos foundPos;
            Direction foundDirection;
            if (unitDirection.z < 0) {
                foundPos = new WorldPos(position.x + currentDistance * unitDirection.x, position.y + currentDistance * unitDirection.y, z - 1).toBlockPos();
                foundDirection = Direction.EAST;
                z--;
            } else {
                foundPos = new WorldPos(position.x + currentDistance * unitDirection.x, position.y + currentDistance * unitDirection.y, z).toBlockPos();
                foundDirection = Direction.WEST;
                z++;
            }

            if (getBlockAt(foundPos) > 0) {
                bestDistance = currentDistance;
                bestDirection = foundDirection;
                bestPos = foundPos;
            }

            currentDistance = (float) distance.applyAsDouble(position.z - z, unitDirection.z);
        }

        if (bestPos == null) return Optional.empty();
        else return Optional.of(new RaytraceResult(bestPos, bestDirection));
    }

    /**
     * Get the position of the nearest block in the given direction from the given position if there is one within maxDistance.
     * @param position    to start from.
     * @param direction   to check toward.
     * @param maxDistance blocks beyond this distance won't be checked.
     * @return Optional with the found position as a value if one was found and Optional.empty() otherwise.
     */
    public Optional<BlockPos> getTargetedBlockPos(WorldPos position, Vector3f direction, float maxDistance) {
        return raytrace(position, direction, maxDistance).map(res -> res.pos);
    }

    /**
     * Get the empty block position next to the targeted block
     * @param position    to start from.
     * @param direction   to check toward.
     * @param maxDistance blocks beyond this distance won't be checked.
     * @return Optional with the found position as a value if one was found and Optional.empty() otherwise.
     */
    public Optional<BlockPos> getTargetedEmptyBlockPos(WorldPos position, Vector3f direction, float maxDistance) {
        return raytrace(position, direction, maxDistance).map(res -> res.pos.toward(res.dir));
    }

    /**
     * Check if the chunk at the given chunkPos is loaded.
     * @param chunkPos the position to check.
     * @return true if loaded, false if not.
     */
    private boolean isChunkLoaded(ChunkPos chunkPos) {
        GridPos gridPos = new GridPos(chunkPos);
        return getGridChunk(gridPos) != null && getGridChunk(gridPos).getPos().equals(chunkPos);
    }

    /**
     * Get the RenderChunk at the given GridPos (position of the chunks[][][] grid).
     * Can return null if there is no loaded chunk at the given GridPos.
     * @param gridPos the position to get.
     * @return the RenderChunk at gridPos if loaded, null otherwise.
     */
    private RenderChunk getGridChunk(GridPos gridPos) {
        return chunks[gridPos.x][gridPos.y][gridPos.z];
    }

    /**
     * Set the RenderChunk at the given GridPos (position of the chunks[][][] grid).
     * @param gridPos the position in the grid to set.
     * @param chunk the RenderChunk to set at the position.
     */
    private void setGridChunk(GridPos gridPos, RenderChunk chunk) {
        chunks[gridPos.x][gridPos.y][gridPos.z] = chunk;
    }

    public void cleanup() {
        for (RenderChunk[][] arr2d : chunks)
            for (RenderChunk[] arr : arr2d)
                for (RenderChunk chunk : arr) {
                    if (chunk != null) chunk.cleanup();
                }
    }

    /**
     * Represents a position of a chunk in the 3d chunks array of this World-object
     */
    private class GridPos {
        public final int x;
        public final int y;
        public final int z;

        /**
         * Creates a new GridPos as a (positive) modulo of the given ChunkPos
         * @param chunkPos the position to convert to GridPos
         */
        public GridPos(ChunkPos chunkPos) {
            int gridSize = renderDistance * 2;
            int tempX = chunkPos.x % gridSize;
            x = tempX < 0 ? tempX + gridSize : tempX;
            int tempY = chunkPos.y % gridSize;
            y = tempY < 0 ? tempY + gridSize : tempY;
            int tempZ = chunkPos.z % gridSize;
            z = tempZ < 0 ? tempZ + gridSize : tempZ;
        }
    }

    private static class RaytraceResult {
        public final BlockPos pos;
        public final Direction dir;

        public RaytraceResult(BlockPos pos, Direction dir) {
            this.pos = pos;
            this.dir = dir;
        }
    }
}
