package me.anttono.bareblocks.client.world;

import me.anttono.bareblocks.api.types.BlockPos;
import me.anttono.bareblocks.api.types.ChunkPos;
import me.anttono.bareblocks.client.ContentLoader;
import me.anttono.bareblocks.client.render.RawMesh;
import me.anttono.bareblocks.templates.BlockTemplate;
import me.anttono.bareblocks.utils.BlockFaceData;
import org.joml.Vector2f;

import java.util.*;

import static me.anttono.bareblocks.utils.DataUtils.toFloatArray;

public class RenderChunk {
    private final World world;
    private final ChunkPos pos;
    private final int[][][] chunkData;
    // Using optional for mesh to easily skip rendering chunks that have no mesh. (completely empty or full)
    private Optional<RawMesh> mesh = Optional.empty();
    private final Vector2f atlasTextureSize;
    private final BlockTemplate[] blockTemplates;

    public RenderChunk(World world, ChunkPos pos, int[][][] blocks, BlockTemplate[] blockTemplates, Vector2f atlasTextureSize) {
        this.world = world;
        this.pos = pos;
        this.chunkData = blocks;
        this.blockTemplates = blockTemplates;
        this.atlasTextureSize = atlasTextureSize;

        this.regenerateMesh();
    }

    /**
     * Generates or regenerates the Mesh for this RenderChunk using the current block data
     */
    public void regenerateMesh() {
        List<Float> positions = new ArrayList<>();
        List<Float> textureCoords = new ArrayList<>();
        List<Float> normals = new ArrayList<>();
        List<Integer> indices = new ArrayList<>();

        // Horizontal faces
        for (int y = 0; y < 16; y++) {
            int yPos = pos.y * 16 + y;
            // Faces perpendicular to x-axis
            for (int z = 0; z < 16; z++) {
                int zPos = pos.z * 16 + z;
                int lastBlock = getWorldBlockAt(new BlockPos(pos.x * 16 - 1, yPos, zPos));
                for (int x = 0; x < 16; x++) {
                    int currBlock = getWorldBlockAt(new BlockPos(pos.x * 16 + x, yPos, zPos));
                    // North face of the previous block
                    if (currBlock == 0 && lastBlock > 0) {
                        int indexOffset = positions.size() / 3;

                        Collections.addAll(positions,
                                0f + x, 0f + y, 1f + z,
                                0f + x, 1f + y, 1f + z,
                                0f + x, 0f + y, 0f + z,
                                0f + x, 1f + y, 0f + z
                        );
                        normals.addAll(BlockFaceData.North.NORMALS);
                        textureCoords.addAll(generateTextureCoords(blockTemplates[lastBlock].getTexture().getNorthTextureId()));
                        BlockFaceData.INDICES.stream().map(val -> val + indexOffset).forEach(indices::add);
                    }
                    // South face of this block
                    if (lastBlock == 0 && currBlock > 0) {
                        int indexOffset = positions.size() / 3;

                        Collections.addAll(positions,
                                0f + x, 0f + y, 0f + z,
                                0f + x, 1f + y, 0f + z,
                                0f + x, 0f + y, 1f + z,
                                0f + x, 1f + y, 1f + z
                        );
                        normals.addAll(BlockFaceData.South.NORMALS);
                        textureCoords.addAll(generateTextureCoords(blockTemplates[currBlock].getTexture().getSouthTextureId()));
                        BlockFaceData.INDICES.stream().map(val -> val + indexOffset).forEach(indices::add);
                    }
                    lastBlock = currBlock;
                }
            }
            // Faces perpendicular to z-axis
            for (int x = 0; x < 16; x++) {
                int xPos = pos.x * 16 + x;
                int lastBlock = getWorldBlockAt(new BlockPos(xPos, yPos, pos.z * 16 - 1));
                for (int z = 0; z < 16; z++) {
                    int currBlock = getWorldBlockAt(new BlockPos(xPos, yPos, pos.z * 16 + z));
                    // East face of the previous block
                    if (currBlock == 0 && lastBlock > 0) {
                        int indexOffset = positions.size() / 3;

                        Collections.addAll(positions,
                                0f + x, 0f + y, 0f + z,
                                0f + x, 1f + y, 0f + z,
                                1f + x, 0f + y, 0f + z,
                                1f + x, 1f + y, 0f + z
                        );
                        normals.addAll(BlockFaceData.East.NORMALS);
                        textureCoords.addAll(generateTextureCoords(blockTemplates[lastBlock].getTexture().getEastTextureId()));
                        BlockFaceData.INDICES.stream().map(val -> val + indexOffset).forEach(indices::add);
                    }
                    // West face of this block
                    if (lastBlock == 0 && currBlock > 0) {
                        int indexOffset = positions.size() / 3;

                        Collections.addAll(positions,
                                1f + x, 0f + y, 0f + z,
                                1f + x, 1f + y, 0f + z,
                                0f + x, 0f + y, 0f + z,
                                0f + x, 1f + y, 0f + z
                        );
                        normals.addAll(BlockFaceData.West.NORMALS);
                        textureCoords.addAll(generateTextureCoords(blockTemplates[currBlock].getTexture().getWestTextureId()));
                        BlockFaceData.INDICES.stream().map(val -> val + indexOffset).forEach(indices::add);
                    }
                    lastBlock = currBlock;
                }
            }
        }
        // Vertical faces (Faces perpendicular to y-axis)
        for (int x = 0; x < 16; x++) {
            int xPos = pos.x * 16 + x;
            for (int z = 0; z < 16; z++) {
                int zPos = pos.z * 16 + z;
                int lastBlock = getWorldBlockAt(new BlockPos(xPos, pos.y * 16 - 1, zPos));
                for (int y = 0; y < 16; y++) {
                    int currBlock = getWorldBlockAt(new BlockPos(xPos, pos.y * 16 + y, zPos));
                    // Top face of the previous block
                    if (currBlock == 0 && lastBlock > 0) {
                        int indexOffset = positions.size() / 3;

                        Collections.addAll(positions,
                                0f + x, 0f + y, 0f + z,
                                1f + x, 0f + y, 0f + z,
                                0f + x, 0f + y, 1f + z,
                                1f + x, 0f + y, 1f + z
                        );
                        normals.addAll(BlockFaceData.Up.NORMALS);
                        textureCoords.addAll(generateTextureCoords(blockTemplates[lastBlock].getTexture().getTopTextureId()));
                        BlockFaceData.INDICES.stream().map(val -> val + indexOffset).forEach(indices::add);
                    }
                    // Bottom face of this block
                    if (lastBlock == 0 && currBlock > 0) {
                        int indexOffset = positions.size() / 3;

                        Collections.addAll(positions,
                                1f + x, 0f + y, 0f + z,
                                0f + x, 0f + y, 0f + z,
                                1f + x, 0f + y, 1f + z,
                                0f + x, 0f + y, 1f + z
                        );
                        normals.addAll(BlockFaceData.Down.NORMALS);
                        textureCoords.addAll(generateTextureCoords(blockTemplates[currBlock].getTexture().getBottomTextureId()));
                        BlockFaceData.INDICES.stream().map(val -> val + indexOffset).forEach(indices::add);
                    }
                    lastBlock = currBlock;
                }
            }
        }

        if (indices.size() == 0) {
            this.mesh = Optional.empty();
        } else {
            RawMesh newMesh = new RawMesh(toFloatArray(positions), toFloatArray(textureCoords), toFloatArray(normals), indices.stream().mapToInt(i -> i).toArray());
            this.mesh = Optional.of(newMesh);
        }
    }


    private List<Float> generateTextureCoords(int textureId) {
        int xPos = textureId % ContentLoader.TEXTURES_PER_ROW;
        int yPos = textureId / ContentLoader.TEXTURES_PER_ROW;

        return Arrays.asList(
                atlasTextureSize.x * xPos, atlasTextureSize.y * (yPos + 1),
                atlasTextureSize.x * xPos, atlasTextureSize.y * yPos,
                atlasTextureSize.x * (xPos + 1), atlasTextureSize.y * (yPos + 1),
                atlasTextureSize.x * (xPos + 1), atlasTextureSize.y * yPos
        );
    }

    public Optional<RawMesh> getMesh() {
        return mesh;
    }

    public ChunkPos getPos() {
        return pos;
    }

    public int[][][] getChunkData() {
        return chunkData;
    }

    /**
     * Gets the block at the given coordinates inside this chunk
     * @param pos position of the block in chunk coordinates
     * @return the id of the block at the given position
     */
    public int getBlockAt(ChunkPos.InternalBlockPos pos) {
        return chunkData[pos.x][pos.y][pos.z];
    }

    public void setBlock(ChunkPos.InternalBlockPos pos, int blockType) {
        chunkData[pos.x][pos.y][pos.z] = blockType;
        this.regenerateMesh();
    }

    /**
     * Gets the block at the given coordinates. If the block is inside this chunk, the block id is read from this chunk's data. Otherwise, the block id is requested from the world
     * @param blockPos position of the block in world coordinates
     * @return the id of the block at the given position. If the requested position is not loaded, -1 will be returned
     */
    public int getWorldBlockAt(BlockPos blockPos) {
        if (pos.containsPos(blockPos)) {
            ChunkPos.InternalBlockPos pos = this.pos.getInternalBlockPos(blockPos);
            return chunkData[pos.x][pos.y][pos.z];
        } else {
            return world.getBlockAt(blockPos);
        }
    }

    /**
     * Clear all off-heap data.
     */
    public void cleanup() {
        this.mesh.ifPresent(RawMesh::deleteBuffers);
    }
}
