package me.anttono.bareblocks.client.io;

import org.joml.Vector2d;
import org.joml.Vector2f;
import org.lwjgl.glfw.GLFWMouseButtonCallbackI;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.glfw.GLFW.*;

public class MouseInput {
    private final List<GLFWMouseButtonCallbackI> keyCallbacks;

    private final Vector2d previousPos;
    private final Vector2d currentPos;

    private final Vector2f displVec;

    private long windowHandle;

    private boolean inWindow = false;

    private boolean leftButtonPressed = false;
    private boolean rightButtonPressed = false;
    private boolean cursorHidden = false;

    public MouseInput() {
        this.keyCallbacks = new ArrayList<>();
        this.previousPos = new Vector2d(-1, -1);
        this.currentPos = new Vector2d(0, 0);
        displVec = new Vector2f();
    }

    public void init(Window window) {
        windowHandle = window.getWindowHandle();
        glfwSetCursorPosCallback(windowHandle, (windowHandle, xPos, yPos) -> {
            currentPos.x = xPos;
            currentPos.y = yPos;
        });
        glfwSetCursorEnterCallback(windowHandle, (windowHandle, entered) -> inWindow = entered);
        glfwSetMouseButtonCallback(windowHandle, (windowHandle, button, action, mode) -> {
            leftButtonPressed = button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS;
            rightButtonPressed = button == GLFW_MOUSE_BUTTON_2 && action == GLFW_PRESS;
            for (GLFWMouseButtonCallbackI callback : keyCallbacks) {
                callback.invoke(windowHandle, button, action, mode);
            }
        });
    }

    public void input() {
        displVec.x = 0;
        displVec.y = 0;
        if (inWindow) {
            double deltaX = currentPos.x - previousPos.x;
            double deltaY = currentPos.y - previousPos.y;
            boolean rotateX = deltaX != 0;
            boolean rotateY = deltaY != 0;
            if (rotateX) {
                displVec.y = (float) deltaX;
            }
            if (rotateY) {
                displVec.x = (float) deltaY;
            }
        }
        previousPos.x = currentPos.x;
        previousPos.y = currentPos.y;
    }

    public void hideCursor() {
        glfwSetInputMode(windowHandle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        cursorHidden = true;
    }

    public void showCursor() {
        glfwSetInputMode(windowHandle, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        cursorHidden = false;
    }

    public boolean isKeyPressed(int keyCode) {
        return glfwGetMouseButton(windowHandle, keyCode) == GLFW_PRESS;
    }

    /**
     * Add a new callback to run when any key is pressed/released.
     * @param callback the callback to run.
     */
    public void addKeyCallback(GLFWMouseButtonCallbackI callback) {
        this.keyCallbacks.add(callback);
    }

    /**
     * Add a new callback to run when the given key is just pressed.
     * @param buttonCode the key to wait for.
     * @param callback the callback to run when the key is just pressed.
     */
    public void addKeyPressCallback(int buttonCode, Runnable callback) {
        this.keyCallbacks.add((windowHandle, button, action, mode) -> {
            if (buttonCode == button && action == GLFW_PRESS) {
                callback.run();
            }
        });
    }

    public Vector2f getDisplVec() {
        return displVec;
    }

    public boolean isLeftButtonPressed() {
        return leftButtonPressed;
    }

    public boolean isRightButtonPressed() {
        return rightButtonPressed;
    }

    public boolean isCursorHidden() {
        return cursorHidden;
    }
}
