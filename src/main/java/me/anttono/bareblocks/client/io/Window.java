package me.anttono.bareblocks.client.io;

import org.joml.Vector2f;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallbackI;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;

public class Window {
    private final List<GLFWKeyCallbackI> keyCallbacks;

    private long windowHandle;

    private int width;
    private int height;
    private String title;

    private boolean resized;

    // PositionAnchor's for HudObjects to use
    private final Vector2f topLeftAnchor;
    private final Vector2f bottomLeftAnchor;
    private final Vector2f topRightAnchor;
    private final Vector2f bottomRightAnchor;
    private final Vector2f centreAnchor;

    public Window(String title) {
        this.keyCallbacks = new ArrayList<>();

        this.title = title;
        this.width = 720;
        this.height = 480;
        this.resized = false;

        this.topLeftAnchor = new Vector2f();
        this.bottomLeftAnchor = new Vector2f();
        this.topRightAnchor = new Vector2f();
        this.bottomRightAnchor = new Vector2f();
        this.centreAnchor = new Vector2f();
    }

    public void init() {
        // Setup error callback
        // Errors will be printed in System.err
        GLFWErrorCallback.createPrint(System.err).set();

        // Initialize glfw
        // Must be done before it can be used
        if (!glfwInit()) {
            throw new IllegalStateException("Unable to initialize GLFW!");
        }

        // Configure window related stuff
        glfwWindowHint(GLFW_VISIBLE, GL_FALSE); // Will stay hidden until setup is finished
        glfwWindowHint(GLFW_RESIZABLE, GL_TRUE); // The window will be resizable
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

        // Create the actual window
        windowHandle = glfwCreateWindow(width, height, title, NULL, NULL);
        if (windowHandle == NULL)
            throw new RuntimeException("Failed to create GLFW window!");

        // Setup resize callback.
        glfwSetFramebufferSizeCallback(this.windowHandle, (window, width, height) ->
        {
            Window.this.width = width;
            Window.this.height = height;
            Window.this.updateAnchors();
            Window.this.setResized(true);
        });

        // Setup key press callbacks
        glfwSetKeyCallback(this.windowHandle, (windowHandle, key, scancode, action, mods) -> {
            for (GLFWKeyCallbackI callback : this.keyCallbacks) {
                callback.invoke(windowHandle, key, scancode, action, mods);
            }
        });

        // Get the resolution of users primary monitor
        GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());

        // Center the window
        glfwSetWindowPos(
                windowHandle,
                (vidMode.width() - width) / 2,
                (vidMode.height() - height) / 2
        );

        // Make the GLFW context current
        glfwMakeContextCurrent(windowHandle);

        // Enable v-sync
        glfwSwapInterval(1);

        // Setup is pretty much done
        // Make the window visible
        glfwShowWindow(windowHandle);

        // Makes LWJGL work properly
        GL.createCapabilities();

        // Set the window clearing colour
        glClearColor(0f, 0f, 0f, 0f);

        // Make the farthest away shapes to be drawn first
        glEnable(GL_DEPTH_TEST);

        // Support for transparency
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        // Face culling: only the sides facing player are rendered = more performance
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
    }

    public void update() {
        glfwSwapBuffers(windowHandle);
        glfwPollEvents();
    }

    public void updateAnchors() {
        bottomLeftAnchor.y = height;
        topRightAnchor.x = width;
        bottomRightAnchor.x = width;
        bottomRightAnchor.y = height;
        centreAnchor.x = width / 2f;
        centreAnchor.y = height / 2f;
    }

    public Vector2f getTopLeftAnchor() {
        return topLeftAnchor;
    }

    public Vector2f getBottomLeftAnchor() {
        return bottomLeftAnchor;
    }

    public Vector2f getTopRightAnchor() {
        return topRightAnchor;
    }

    public Vector2f getBottomRightAnchor() {
        return bottomRightAnchor;
    }

    public Vector2f getCentreAnchor() {
        return centreAnchor;
    }

    public void setClearColour(float r, float g, float b, float alpha) {
        glClearColor(r, g, b, alpha);
    }

    public void setResized(boolean resized) {
        this.resized = resized;
    }

    public boolean isResized() {
        return this.resized;
    }

    public boolean shouldClose() {
        return glfwWindowShouldClose(windowHandle);
    }

    public long getWindowHandle() {
        return this.windowHandle;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    // Key stuff
    public boolean isKeyPressed(int keyCode) {
        return glfwGetKey(windowHandle, keyCode) == GLFW_PRESS;
    }

    /**
     * Add a new callback to run when any key is pressed/released.
     * @param callback the callback to run.
     */
    public void addKeyCallback(GLFWKeyCallbackI callback) {
        this.keyCallbacks.add(callback);
    }

    /**
     * Add a new callback to run when the given key is just pressed.
     * @param keyCode the key to wait for.
     * @param callback the callback to run when the key is just pressed.
     */
    public void addKeyPressCallback(int keyCode, Runnable callback) {
        this.keyCallbacks.add((windowHandle, key, scancode, action, mods) -> {
            if (key == keyCode && action == GLFW_PRESS) {
                callback.run();
            }
        });
    }
}
