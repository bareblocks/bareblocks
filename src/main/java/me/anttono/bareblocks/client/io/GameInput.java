package me.anttono.bareblocks.client.io;

import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.glfw.GLFW.GLFW_PRESS;

public class GameInput {
    private final List<Input> inputs;
    private final Window window;
    private final MouseInput mouseInput;
    private boolean active;

    public GameInput(Window window, MouseInput mouseInput) {
        this.inputs = new ArrayList<>();
        this.window = window;
        this.mouseInput = mouseInput;
        this.active = false;
    }

    public Input newInput(int keyCode, boolean isMouseButton) {
        Input input = new Input(keyCode, isMouseButton);
        this.inputs.add(input);
        return input;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isCursorHidden() {
        return mouseInput.isCursorHidden();
    }

    public Vector2f getMouseDisplVec() {
        return mouseInput.getDisplVec();
    }

    public class Input {
        private int keyCode;
        private boolean isMouseButton;
        private boolean justPressed;

        public Input(int keyCode, boolean isMouseButton) {
            this.keyCode = keyCode;
            this.isMouseButton = isMouseButton;

            // Callbacks for "key just pressed"-event
            window.addKeyCallback((windowHandle, key, scancode, action, mods) ->  {
                if (active && !this.isMouseButton && key == this.keyCode && action == GLFW_PRESS) {
                    justPressed = true;
                }
            });
            mouseInput.addKeyCallback((windowHandle, button, action, mode) -> {
                if (active && this.isMouseButton && button == this.keyCode && action == GLFW_PRESS) {
                    justPressed = true;
                }
            });
        }

        /**
         * Check if this input is currently active.
         * @return true if the input is active and the game is active, false otherwise.
         */
        public boolean isActive() {
            if (!active) return false;
            if (isMouseButton) {
                return mouseInput.isKeyPressed(keyCode);
            } else {
                return window.isKeyPressed(keyCode);
            }
        }

        /**
         * Check if this input was just activated since last checked. Will return true on the first call
         * and false on all following calls until the input is activated again.
         * @return true if the input has been activated after last call, false otherwise.
         */
        public boolean isJustPressed() {
            if (!active) return false;
            boolean wasJustPressed = justPressed;
            justPressed = false;
            return wasJustPressed;
        }
    }
}
