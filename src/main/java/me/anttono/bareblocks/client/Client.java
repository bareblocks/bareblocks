package me.anttono.bareblocks.client;

import me.anttono.bareblocks.api.types.BlockPos;
import me.anttono.bareblocks.client.io.GameInput;
import me.anttono.bareblocks.client.io.MouseInput;
import me.anttono.bareblocks.client.io.Window;
import me.anttono.bareblocks.client.render.Renderer;
import me.anttono.bareblocks.client.render.Texture;
import me.anttono.bareblocks.client.render.hud.FontTexture;
import me.anttono.bareblocks.client.render.hud.Hud;
import me.anttono.bareblocks.client.render.hud.objects.HudObject;
import me.anttono.bareblocks.client.render.lights.DirectionalLight;
import me.anttono.bareblocks.client.render.lights.PointLight;
import me.anttono.bareblocks.client.render.lights.SceneLight;
import me.anttono.bareblocks.client.render.lights.SpotLight;
import me.anttono.bareblocks.client.render.objects.TextObject;
import me.anttono.bareblocks.client.world.World;
import me.anttono.bareblocks.utils.ErrorLogger;
import me.anttono.bareblocks.utils.HudUtils;
import me.anttono.bareblocks.utils.Timer;
import me.anttono.bareblocks.utils.threading.requests.BlockBreakRequest;
import me.anttono.bareblocks.utils.threading.requests.BlockPlaceRequest;
import me.anttono.bareblocks.utils.threading.requests.Request;
import me.anttono.bareblocks.utils.threading.requests.RequestType;
import org.joml.Vector3f;
import org.joml.Vector4f;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.lwjgl.glfw.GLFW.*;

public class Client implements Runnable {
    public static final int TARGET_UPS = 60;

    /*
        System and rendering related variables
     */
    private final Thread clientThread;

    private final Window window;
    private final Timer timer;
    private final Renderer renderer;
    private final MouseInput mouseInput;

    // The game
    private GameInput currentGameInput;
    private Game currentGame;


    public Client(Window window) {
        this.clientThread = new Thread(this, "CLIENT_THREAD");

        this.window = window;

        this.timer = new Timer();
        this.renderer = new Renderer();
        this.mouseInput = new MouseInput();
    }

    public void start() {
        clientThread.start();
    }

    @Override
    public void run() {
        System.out.println("Client is running");

        try {
            this.init();
            this.renderLoop();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cleanup();
            ErrorLogger.save();
        }
    }

    protected void init() throws Exception {
        // Window must be initialized here in the new thread
        window.init();

        timer.init();
        renderer.init(window);
        mouseInput.init(window);

        // Setup all "key just pressed" events
        window.addKeyPressCallback(GLFW_KEY_ESCAPE, this::toggleGameActive);

        // Start a game
        currentGameInput = new GameInput(window, mouseInput);
        currentGame = new Game(currentGameInput, window);
    }

    protected void renderLoop() {
        // Clients main loop
        // Holds a steady ups defined with TARGET_UPS
        // The rest of the work is done by other methods (and timer)
        float elapsedTime;
        float accumulator = 0f;
        float interval = 1f / TARGET_UPS;

        boolean running = true;
        while (running && !window.shouldClose()) {
            elapsedTime = timer.getElapsedTime();
            accumulator += elapsedTime;

            input();

            while (accumulator >= interval) {
                update();
                accumulator -= interval;
            }

            render();
        }

        System.out.println("Client stopped.");
    }

    protected void input() {
        // Mouse input
        mouseInput.input();

        if (isInGame()) {
            currentGame.input();
        }
    }

    protected void update() {
        // This makes timer count ups
        timer.addUpdate();

        if (isInGame()) {
            currentGame.update(timer.getFps(), timer.getUps());
        }
    }

    protected void render() {
        // This makes timer count fps
        timer.addFrame();

        if (isInGame()) {
            currentGame.render(renderer, window);
        }

        window.update();
    }

    protected boolean isInGame() {
        return this.currentGame != null;
    }

    protected void toggleGameActive() {
        if (mouseInput.isCursorHidden()) {
            mouseInput.showCursor();
            if (isInGame()) {
                currentGameInput.setActive(false);
            }
        } else {
            mouseInput.hideCursor();
            if (isInGame()) {
                currentGameInput.setActive(true);
            }
        }
    }

    protected void cleanup() {
        if (isInGame()) {
            currentGame.stop();
        }

        renderer.cleanup();
    }
}
