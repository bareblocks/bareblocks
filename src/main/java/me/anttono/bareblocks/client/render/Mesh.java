package me.anttono.bareblocks.client.render;

import static org.lwjgl.opengl.GL30.*;

/**
 * A Mesh that has material.
 */
public class Mesh extends RawMesh {
    private Material material;

    public Mesh(float[] positions, float[] textCoords, float[] normals, int[] indices) {
        super(positions, textCoords, normals, indices);
    }

    protected void initRender() {
        // Use texture if provided
        material.getTexture().ifPresent(texture -> {
            // Activate texture unit
            glActiveTexture(GL_TEXTURE0);
            // Bind the texture
            glBindTexture(GL_TEXTURE_2D, texture.getId());
        });

        super.initRender();
    }

    protected void endRender() {
        // Restore state
        super.endRender();
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    /**
     * Clean up all off-heap data related to this mesh.
     */
    public void cleanup() {
        glDisableVertexAttribArray(0);

        // Delete VBOs
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        for (int vboId : vboIdList) {
            glDeleteBuffers(vboId);
        }
        glDeleteBuffers(indexVboId);

        // Delete texture
        if (material != null) {
            material.getTexture().ifPresent(Texture::cleanup);
        }

        // Delete VAO
        glBindVertexArray(0);
        glDeleteVertexArrays(vaoId);
    }
}
