package me.anttono.bareblocks.client.render.lights;

import org.joml.Vector3f;

public class DirectionalLight {
    private Vector3f direction;

    private Vector3f colour;
    private float intensity;

    public DirectionalLight(Vector3f direction, Vector3f colour, float intensity) {
        this.direction = direction;
        this.colour = colour;
        this.intensity = intensity;
    }

    public DirectionalLight(DirectionalLight light) {
        this(new Vector3f(light.getDirection()), new Vector3f(light.getColour()), light.getIntensity());
    }

    public Vector3f getDirection() {
        return direction;
    }

    public void setDirection(Vector3f direction) {
        this.direction = direction;
    }

    public Vector3f getColour() {
        return colour;
    }

    public void setColour(Vector3f colour) {
        this.colour = colour;
    }

    public float getIntensity() {
        return intensity;
    }

    public void setIntensity(float intensity) {
        this.intensity = intensity;
    }
}
