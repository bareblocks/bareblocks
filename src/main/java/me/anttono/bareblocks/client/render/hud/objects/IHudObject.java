package me.anttono.bareblocks.client.render.hud.objects;


import me.anttono.bareblocks.client.render.objects.RenderObject;

import java.util.List;

// The basic stuff all the hudObjects need in order to be usable from Hud class
public interface IHudObject {
    // Used to define position along z-axis so that z-coordinate equals (int) layer * LAYER_STEP
    float LAYER_STEP = 1f / 256f;

    // Should be called always when the window dimensions change
    // Should make the object update its own position according to the change in screen dimensions
    void updatePos();

    // It doesn't matter how the list is made or what it contains. It can even be empty if necessary
    List<RenderObject> getRenderObjects();

    // This should call cleanup for all RenderObjects and other possible objects that need it in this class
    void cleanup();
}
