package me.anttono.bareblocks.client.render.hud.objects;

import me.anttono.bareblocks.client.render.objects.RenderObject;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.List;

public class HudObject implements IHudObject {
    // The actual visible object
    private final RenderObject renderObject;

    // Object dimensions
    private float width;
    private float height;

    // Way to control position for this HudObject
    // PositionAnchor is a Vector, whose value is defined and updated in some other place and used here
    private Vector2f positionAnchor;
    // PositionShift is this object's position relative to positionAnchor
    private final Vector2f positionShift;
    // Layer is integer used to define position along z-axis. Elements with higher values are drawn over elements with smaller ones
    private int layer;

    // PositionAnchor's for other elements to use
    private final Vector2f topLeftAnchor;
    private final Vector2f bottomLeftAnchor;
    private final Vector2f topRightAnchor;
    private final Vector2f bottomRightAnchor;
    private final Vector2f centreAnchor;

    public HudObject(RenderObject renderObject, float width, float height) {
        this.renderObject = renderObject;
        this.width = width;
        this.height = height;
        this.layer = 0;
        this.positionShift = new Vector2f();

        this.topLeftAnchor = new Vector2f();
        this.bottomLeftAnchor = new Vector2f();
        this.topRightAnchor = new Vector2f();
        this.bottomRightAnchor = new Vector2f();
        this.centreAnchor = new Vector2f();
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }

    public int getLayer() {
        return layer;
    }

    public void setWidth(float width) {
        this.width = width;
        updateAnchors();
    }

    public Vector2f getTopLeftAnchor() {
        return topLeftAnchor;
    }

    public Vector2f getBottomLeftAnchor() {
        return bottomLeftAnchor;
    }

    public Vector2f getTopRightAnchor() {
        return topRightAnchor;
    }

    public Vector2f getBottomRightAnchor() {
        return bottomRightAnchor;
    }

    public Vector2f getCentreAnchor() {
        return centreAnchor;
    }

    public void setHeight(float height) {
        this.height = height;
        updateAnchors();
    }

    public void setPositionShift(float x, float y) {
        this.positionShift.x = x;
        this.positionShift.y = y;
        updateAnchors();
    }

    public void setPositionAnchor(Vector2f positionAnchor) {
        this.positionAnchor = positionAnchor;
        updateAnchors();
    }

    private void updateAnchors() {
        topLeftAnchor.x = positionAnchor.x + positionShift.x;
        topLeftAnchor.y = positionAnchor.y + positionShift.y;

        bottomLeftAnchor.x = topLeftAnchor.x;
        bottomLeftAnchor.y = topLeftAnchor.y + height;
        topRightAnchor.x = topLeftAnchor.x + width;
        topRightAnchor.y = topLeftAnchor.y;
        bottomRightAnchor.x = topLeftAnchor.x + width;
        bottomRightAnchor.y = topLeftAnchor.y + height;

        centreAnchor.x = topLeftAnchor.x + width / 2f;
        centreAnchor.y = topLeftAnchor.y + height / 2f;
    }

    @Override
    public void updatePos() {
        updateAnchors();
        this.renderObject.setPosition(topLeftAnchor.x, topLeftAnchor.y, (float) layer * LAYER_STEP);
    }

    @Override
    public List<RenderObject> getRenderObjects() {
        List<RenderObject> list = new ArrayList<>();
        list.add(renderObject);
        return list;
    }

    @Override
    public void cleanup() {
        renderObject.getMesh().cleanup();
    }
}