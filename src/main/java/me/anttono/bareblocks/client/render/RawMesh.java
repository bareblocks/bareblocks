package me.anttono.bareblocks.client.render;

import me.anttono.bareblocks.client.render.objects.RenderObject;
import org.lwjgl.system.MemoryUtil;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

/**
 * A Mesh with no material.
 */
public class RawMesh {
    protected final int vaoId;
    protected final List<Integer> vboIdList;
    protected final int indexVboId;

    protected final int vertexCount;

    /**
     * RawMesh constructor designed for textured 3d-models with normals defined.
     * @param positions  3-dimensional vertex coordinates
     * @param textCoords 2-dimensional texture coordinates
     * @param normals    3-dimensional vertex normals
     * @param indices    indices for the actual mesh in groups of three per triangle
     */
    public RawMesh(float[] positions, float[] textCoords, float[] normals, int[] indices) {
        this.vertexCount = indices.length;
        this.vboIdList = new ArrayList<>();

        // Create VAO
        vaoId = glGenVertexArrays();
        glBindVertexArray(vaoId);

        // Create VBOs
        createVbo(positions, 3);
        createVbo(textCoords, 2);
        createVbo(normals, 3);
        indexVboId = createIndexVbo(indices);

        // Unbind VAO and VBO
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    public RawMesh(float[] positions, int[] indices) {
        this.vertexCount = indices.length;
        this.vboIdList = new ArrayList<>();

        // Create VAO
        vaoId = glGenVertexArrays();
        glBindVertexArray(vaoId);

        // Create VBOs
        createVbo(positions, 3);
        indexVboId = createIndexVbo(indices);

        // Unbind VAO and VBO
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    /**
     * Create a new VBO for this mesh with the given floating point data.
     * ASSUMES THE CORRECT VAO AND VBO ARE ALREADY BOUND!
     * @param data        the data to write to the VBO
     * @param elementSize defines how many elements in the array are related to a single vertex
     */
    private void createVbo(float[] data, int elementSize) {
        FloatBuffer dataBuffer = null;
        try {
            int attribArrayIndex = vboIdList.size();

            int vboId = glGenBuffers();
            vboIdList.add(vboId);
            dataBuffer = MemoryUtil.memAllocFloat(data.length);
            dataBuffer.put(data).flip();
            glBindBuffer(GL_ARRAY_BUFFER, vboId);
            glBufferData(GL_ARRAY_BUFFER, dataBuffer, GL_STATIC_DRAW);
            glVertexAttribPointer(attribArrayIndex, elementSize, GL_FLOAT, false, 0, 0);
        } finally {
            if (dataBuffer != null) {
                MemoryUtil.memFree(dataBuffer);
            }
        }
    }

    /**
     * Create a indices VBO for this mesh with the given index data.
     * ASSUMES THE CORRECT VAO AND VBO ARE ALREADY BOUND!
     * @param data the index data to write.
     * @return the id of the created VBO.
     */
    private int createIndexVbo(int[] data) {
        IntBuffer dataBuffer = null;
        int vboId = glGenBuffers();
        try {
            // Create indices VBO
            dataBuffer = MemoryUtil.memAllocInt(data.length);
            dataBuffer.put(data).flip();
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, dataBuffer, GL_STATIC_DRAW);
        } finally {
            if (dataBuffer != null) {
                MemoryUtil.memFree(dataBuffer);
            }
        }
        return vboId;
    }

    protected void initRender() {
        // Bind the buffers for this object
        glBindVertexArray(getVaoId());
        for (int i = 0; i < vboIdList.size(); i++) {
            glEnableVertexAttribArray(i);
        }
    }

    protected void endRender() {
        // Restore state
        for (int i = 0; i < vboIdList.size(); i++) {
            glDisableVertexAttribArray(i);
        }
        glBindVertexArray(0);
    }

    /**
     * Render this mesh as a solid object consisting of triangles.
     */
    public void renderSolid() {
        initRender();

        glDrawElements(GL_TRIANGLES, vertexCount, GL_UNSIGNED_INT, 0);

        endRender();
    }

    /**
     * Render this mesh as a wireframe consisting of lines between two points.
     */
    public void renderWireframe() {
        initRender();

        glDrawElements(GL_LINES, vertexCount, GL_UNSIGNED_INT, 0);

        endRender();
    }

    public void renderList(List<RenderObject> renderObjects, Consumer<RenderObject> consumer) {
        initRender();

        for (RenderObject object : renderObjects) {
            // Set up the data required by this RenderObject
            consumer.accept(object);
            // Render this RenderObject
            glDrawElements(GL_TRIANGLES, getVertexCount(), GL_UNSIGNED_INT, 0);
        }

        endRender();
    }

    public int getVaoId() {
        return vaoId;
    }

    public int getVertexCount() {
        return vertexCount;
    }

    /**
     * Clean up all off-heap data except the Material related to this mesh.
     */
    public void deleteBuffers() {
        glDisableVertexAttribArray(0);

        // Delete VBOs
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        for (int vboId : vboIdList) {
            glDeleteBuffers(vboId);
        }
        glDeleteBuffers(indexVboId);

        // Delete VAO
        glBindVertexArray(0);
        glDeleteVertexArrays(vaoId);
    }
}
