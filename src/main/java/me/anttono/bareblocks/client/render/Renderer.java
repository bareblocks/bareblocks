package me.anttono.bareblocks.client.render;

import me.anttono.bareblocks.api.types.BlockPos;
import me.anttono.bareblocks.client.io.Window;
import me.anttono.bareblocks.client.render.hud.Hud;
import me.anttono.bareblocks.client.render.lights.DirectionalLight;
import me.anttono.bareblocks.client.render.lights.PointLight;
import me.anttono.bareblocks.client.render.lights.SceneLight;
import me.anttono.bareblocks.client.render.lights.SpotLight;
import me.anttono.bareblocks.client.render.objects.RenderObject;
import me.anttono.bareblocks.client.world.RenderChunk;
import me.anttono.bareblocks.client.world.World;
import me.anttono.bareblocks.utils.BlockFaceData;
import me.anttono.bareblocks.utils.FileUtils;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import java.util.Optional;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;

public class Renderer {
    private static final float FOV = (float) Math.toRadians(60.0f);
    private static final float Z_NEAR = 0.01f;
    private static final float Z_FAR = 1000.0f;

    private static final int MAX_POINT_LIGHTS = 5;
    private static final int MAX_SPOT_LIGHTS = 5;

    private final Transformation transformation;

    private ShaderProgram sceneShaderProgram;
    private ShaderProgram hudShaderProgram;
    private ShaderProgram targetShaderProgram;

    private float specularPower;

    private RawMesh targetWireframe;

    public Renderer() {
        transformation = new Transformation();
        specularPower = 10f;
    }

    public void init(Window window) throws Exception {
        setupSceneShader();
        setupHudShader();
        setupTargetShader();

        targetWireframe = new RawMesh(BlockFaceData.blockWireframePositions, BlockFaceData.blocWireframeIndices);

        window.setClearColour(0f, 0f, 0f, 1f);
    }

    private void setupSceneShader() throws Exception {
        // Prepare this shaderProgram with the correct shaders
        sceneShaderProgram = new ShaderProgram();
        sceneShaderProgram.createVertexShader(FileUtils.loadResource("/shaders/scene_vertex.vs"));
        sceneShaderProgram.createFragmentShader(FileUtils.loadResource("/shaders/scene_fragment.fs"));
        sceneShaderProgram.link();

        // Matrices that make the world look correct from current location
        sceneShaderProgram.createUniform("projectionMatrix");
        sceneShaderProgram.createUniform("modelViewMatrix");

        // Thing that makes textures work
        sceneShaderProgram.createUniform("texture_sampler");

        // Uniform for material(s)
        sceneShaderProgram.createMaterialUniform("material");

        // Lighting related uniforms
        sceneShaderProgram.createUniform("specularPower");
        sceneShaderProgram.createUniform("ambientLight");
        sceneShaderProgram.createPointLightListUniform("pointLights", MAX_POINT_LIGHTS);
        sceneShaderProgram.createSpotLightListUniform("spotLights", MAX_SPOT_LIGHTS);
        sceneShaderProgram.createDirectionalLightUniform("directionalLight");
    }

    private void setupHudShader() throws Exception {
        // Prepare this shaderProgram with the correct shaders
        hudShaderProgram = new ShaderProgram();
        hudShaderProgram.createVertexShader(FileUtils.loadResource("/shaders/hud_vertex.vs"));
        hudShaderProgram.createFragmentShader(FileUtils.loadResource("/shaders/hud_fragment.fs"));
        hudShaderProgram.link();

        // Create uniforms for orthographic projection matrix and base colour
        hudShaderProgram.createUniform("projModelMatrix");
        hudShaderProgram.createUniform("colour");
        hudShaderProgram.createUniform("hasTexture");
    }

    private void setupTargetShader() throws Exception {
        // Load the shaders for this ShaderProgram
        targetShaderProgram = new ShaderProgram();
        targetShaderProgram.createVertexShader(FileUtils.loadResource("/shaders/target_vertex.vs"));
        targetShaderProgram.createFragmentShader(FileUtils.loadResource("/shaders/target_fragment.fs"));
        targetShaderProgram.link();

        targetShaderProgram.createUniform("projectionMatrix");
        targetShaderProgram.createUniform("modelViewMatrix");
    }

    /**
     * Start rendering a new frame. The window will be cleared and resized, and a scene from the given world will be
     * drawn on the window.
     * @param window the window to render into.
     * @param camera a camera to use for rendering a view into the world.
     * @param world the world to render a scene from.
     * @param sceneLight lights present in the world.
     * @param textureAtlasMaterial a material with the texture atlas to use for chunk rendering.
     */
    public void render(Window window, Camera camera, World world, SceneLight sceneLight, Material textureAtlasMaterial) {
        clear();

        if (window.isResized()) {
            glViewport(0, 0, window.getWidth(), window.getHeight());
            window.setResized(false);
        }

        renderScene(window, camera, world, sceneLight, textureAtlasMaterial);
    }

    /**
     * Render a scene from the world.
     */
    private void renderScene(Window window, Camera camera, World world, SceneLight sceneLight, Material textureAtlasMaterial) {
        sceneShaderProgram.bind();

        // Update projection Matrix
        Matrix4f projectionMatrix = transformation.getProjectionMatrix(FOV, window.getWidth(), window.getHeight(), Z_NEAR, Z_FAR);
        sceneShaderProgram.setUniform("projectionMatrix", projectionMatrix);

        // Update view Matrix
        Matrix4f viewMatrix = transformation.getViewMatrix(camera);

        renderLights(viewMatrix, sceneLight);

        // Prepare the block texture atlas
        sceneShaderProgram.setUniform("texture_sampler", 0);
        sceneShaderProgram.setUniform("material", textureAtlasMaterial);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureAtlasMaterial.getTexture().map(Texture::getId).orElse(0));
        // Render all chunks
        for (RenderChunk[][] arr2d : world.getChunks())
            for (RenderChunk[] arr : arr2d)
                for (RenderChunk chunk : arr) {
                    if (chunk == null) continue;
                    chunk.getMesh().ifPresent(mesh -> {
                        // Set model view Matrix for this object
                        Matrix4f modelViewMatrix = transformation.getModelViewMatrix(chunk, viewMatrix);
                        sceneShaderProgram.setUniform("modelViewMatrix", modelViewMatrix);
                        // Do the rendering
                        mesh.renderSolid();
                    });
                }
        // Unbind the texture
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, 0);

        sceneShaderProgram.unbind();

        // Render the targeted block
        targetShaderProgram.bind();

        Optional<BlockPos> targetPos = world.getTargetedBlockPos(
                camera.getPosition(),
                camera.getFacingDirection(),
                World.MAX_INTERACT_DISTANCE);
        targetPos.ifPresent(pos -> {
            Matrix4f modelViewMatrix = transformation.getModelViewMatrix(pos.toVector(), new Vector3f(), 1f, viewMatrix);
            targetShaderProgram.setUniform("projectionMatrix", projectionMatrix);
            targetShaderProgram.setUniform("modelViewMatrix", modelViewMatrix);
            targetWireframe.renderWireframe();
        });

        targetShaderProgram.unbind();
    }

    /**
     * Prepare the lights in the scene before rendering a scene from the world.
     */
    private void renderLights(Matrix4f viewMatrix, SceneLight sceneLight) {
        // Update ambient light
        sceneShaderProgram.setUniform("ambientLight", sceneLight.getAmbientLight());
        sceneShaderProgram.setUniform("specularPower", specularPower);

        // Loop through all pointLights
        PointLight[] pointLights = sceneLight.getPointLights();
        int numPointLights = pointLights != null ? pointLights.length : 0;
        for (int i = 0; i < numPointLights; i++) {
            // Copy the PointLight and transform its position to view coordinates
            PointLight currPointLight = new PointLight(pointLights[i]);
            Vector3f lightPos = currPointLight.getPosition();
            Vector4f aux = new Vector4f(lightPos, 1);
            aux.mul(viewMatrix);
            lightPos.x = aux.x;
            lightPos.y = aux.y;
            lightPos.z = aux.z;

            // Update this pointLight using the transformed position
            sceneShaderProgram.setUniform("pointLights", currPointLight, i);
        }

        // Loop through all spotLights
        SpotLight[] spotLights = sceneLight.getSpotLights();
        int numSpotLights = spotLights != null ? spotLights.length : 0;
        for (int i = 0; i < numSpotLights; i++) {
            // Copy the spotLight and transform its position and cone direction to view coordinates
            SpotLight currSpotLight = new SpotLight(spotLights[i]);
            Vector4f dir = new Vector4f(currSpotLight.getConeDirection(), 0);
            dir.mul(viewMatrix);
            currSpotLight.setConeDirection(new Vector3f(dir.x, dir.y, dir.z));

            Vector3f spotLightPos = currSpotLight.getPointLight().getPosition();
            Vector4f auxSpot = new Vector4f(spotLightPos, 1);
            auxSpot.mul(viewMatrix);
            spotLightPos.x = auxSpot.x;
            spotLightPos.y = auxSpot.y;
            spotLightPos.z = auxSpot.z;

            // Update this spotlight using the transformed position
            sceneShaderProgram.setUniform("spotLights", currSpotLight, i);
        }

        // Copy the directional light ant transform it's position to view coordinates
        DirectionalLight currDirLight = new DirectionalLight(sceneLight.getDirectionalLight());
        Vector4f dir = new Vector4f(currDirLight.getDirection(), 0);
        dir.mul(viewMatrix);
        currDirLight.setDirection(new Vector3f(dir.x, dir.y, dir.z));
        sceneShaderProgram.setUniform("directionalLight", currDirLight);
    }

    /**
     * Render the given Hud on top of the currently rendered scene and possible previous Huds.
     * @param window the window to render into.
     * @param hud the Hud to render.
     */
    public void renderHud(Window window, Hud hud) {
        hudShaderProgram.bind();

        Matrix4f ortho = transformation.getOrthoProjectionMatrix(0, window.getWidth(), window.getHeight(), 0);
        for (RenderObject renderObject : hud.getRenderObjects()) {
            Mesh mesh = renderObject.getMesh();
            // Set orthographic and model matrix for this HUD object
            Matrix4f projModelMatrix = transformation.getOrtoProjModelMatrix(renderObject, ortho);
            hudShaderProgram.setUniform("projModelMatrix", projModelMatrix);
            hudShaderProgram.setUniform("colour", mesh.getMaterial().getAmbientColour());
            hudShaderProgram.setUniform("hasTexture", mesh.getMaterial().isTextured() ? 1 : 0);

            // Render the mesh for this HUD object
            mesh.renderSolid();
        }

        hudShaderProgram.unbind();
    }

    public void clear() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    public void cleanup() {
        if (sceneShaderProgram != null) {
            sceneShaderProgram.cleanup();
        }
        if (hudShaderProgram != null) {
            hudShaderProgram.cleanup();
        }
        if (targetShaderProgram != null) {
            targetShaderProgram.cleanup();
        }
        targetWireframe.deleteBuffers();
    }
}
