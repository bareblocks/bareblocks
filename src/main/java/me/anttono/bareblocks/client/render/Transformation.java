package me.anttono.bareblocks.client.render;

import me.anttono.bareblocks.api.types.WorldPos;
import me.anttono.bareblocks.client.render.objects.RenderObject;
import me.anttono.bareblocks.client.world.RenderChunk;
import org.joml.Matrix4f;
import org.joml.Vector3f;

public class Transformation {
    private final Matrix4f projectionMatrix;
    private final Matrix4f modelViewMatrix;
    private final Matrix4f viewMatrix;
    private final Matrix4f orthoMatrix;

    public Transformation() {
        this.modelViewMatrix = new Matrix4f();
        this.projectionMatrix = new Matrix4f();
        this.viewMatrix = new Matrix4f();
        this.orthoMatrix = new Matrix4f();
    }

    public final Matrix4f getProjectionMatrix(float fov, float width, float height, float zNear, float zFar) {
        float aspectRatio = width / height;
        projectionMatrix.identity();
        projectionMatrix.perspective(fov, aspectRatio, zNear, zFar);
        return projectionMatrix;
    }

    public Matrix4f getModelViewMatrix(Vector3f position, Vector3f rotation, float scale, Matrix4f viewMatrix) {
        modelViewMatrix.identity().
                translate(position).
                rotateX((float) Math.toRadians(rotation.x)).
                rotateY((float) Math.toRadians(rotation.y)).
                rotateZ((float) Math.toRadians(rotation.z)).
                scale(scale);
        Matrix4f viewCurr = new Matrix4f(viewMatrix);
        return viewCurr.mul(modelViewMatrix);
    }

    public Matrix4f getModelViewMatrix(RenderObject object, Matrix4f viewMatrix) {
        return getModelViewMatrix(object.getPosition(), object.getRotation(), object.getScale(), viewMatrix);
    }

    public Matrix4f getModelViewMatrix(RenderChunk chunk, Matrix4f viewMatrix) {
        return getModelViewMatrix(chunk.getPos().toVector(), new Vector3f(), 1f, viewMatrix);
    }

    public Matrix4f getViewMatrix(Camera camera) {
        WorldPos cameraPos = camera.getPosition();
        Vector3f cameraRot = camera.getRotation();

        viewMatrix.identity();
        // Rotation first, because it may affect translation
        viewMatrix
                .rotate((float) Math.toRadians(cameraRot.x), new Vector3f(1, 0, 0))
                .rotate((float) Math.toRadians(cameraRot.y), new Vector3f(0, 1, 0));
        // Then translation
        viewMatrix.translate(-cameraPos.x, -cameraPos.y, -cameraPos.z);

        return viewMatrix;
    }

    public Matrix4f getOrthoProjectionMatrix(float left, float right, float bottom, float top) {
        orthoMatrix.identity();
        orthoMatrix.setOrtho2D(left, right, bottom, top);
        return orthoMatrix;
    }

    public Matrix4f getOrtoProjModelMatrix(RenderObject renderObject, Matrix4f orthoMatrix) {
        Vector3f rotation = renderObject.getRotation();
        Matrix4f modelMatrix = new Matrix4f();
        modelMatrix.identity().translate(renderObject.getPosition())
                .rotateX((float) Math.toRadians(-rotation.x))
                .rotateY((float) Math.toRadians(-rotation.y))
                .rotateZ((float) Math.toRadians(-rotation.z))
                .scale(renderObject.getScale());
        Matrix4f orthoMatrixCurr = new Matrix4f(orthoMatrix);
        orthoMatrixCurr.mul(modelMatrix);
        return orthoMatrixCurr;
    }
}
