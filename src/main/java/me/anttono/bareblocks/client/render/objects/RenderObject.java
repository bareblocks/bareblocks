package me.anttono.bareblocks.client.render.objects;

import me.anttono.bareblocks.client.render.Mesh;
import org.joml.Vector3f;

public class RenderObject {
    private Mesh mesh;

    private final Vector3f position;
    private final Vector3f rotation;
    private float scale;

    public RenderObject() {
        position = new Vector3f();
        rotation = new Vector3f();
        scale = 1f;
    }

    public RenderObject(Mesh mesh) {
        this();
        this.mesh = mesh;
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(float x, float y, float z) {
        this.position.x = x;
        this.position.y = y;
        this.position.z = z;
    }

    public Vector3f getRotation() {
        return rotation;
    }

    public void setRotation(float x, float y, float z) {
        this.rotation.x = x;
        this.rotation.y = y;
        this.rotation.z = z;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public Mesh getMesh() {
        return mesh;
    }

    public void setMesh(Mesh mesh) {
        this.mesh = mesh;
    }
}
