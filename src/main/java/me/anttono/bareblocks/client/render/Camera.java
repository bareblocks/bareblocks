package me.anttono.bareblocks.client.render;

import me.anttono.bareblocks.api.types.WorldPos;
import org.joml.Vector3f;

public class Camera {
    private final WorldPos position;
    private final Vector3f rotation;

    private final Vector3f facingDirection;

    public Camera() {
        this.position = new WorldPos();
        this.rotation = new Vector3f(0, 0, 0);

        this.facingDirection = calcFacingDirection(rotation);
    }

    public Camera(WorldPos position, Vector3f rotation) {
        this.position = position;
        this.rotation = rotation;

        this.facingDirection = calcFacingDirection(rotation);
    }

    public WorldPos getPosition() {
        return position;
    }

    public void setPosition(float x, float y, float z) {
        position.x = x;
        position.y = y;
        position.z = z;
    }

    public void movePosition(float offsetX, float offsetY, float offsetZ) {
        if (offsetZ != 0) {
            position.x += (float) Math.sin(Math.toRadians(rotation.y)) * -1.0f * offsetZ;
            position.z += (float) Math.cos(Math.toRadians(rotation.y)) * offsetZ;
        }
        if (offsetX != 0) {
            position.x += (float) Math.sin(Math.toRadians(rotation.y - 90)) * -1.0f * offsetX;
            position.z += (float) Math.cos(Math.toRadians(rotation.y - 90)) * offsetX;
        }
        position.y += offsetY;
    }

    public Vector3f getRotation() {
        return rotation;
    }

    public void setRotation(float x, float y, float z) {
        rotation.x = x;
        rotation.y = y;
        rotation.z = z;

        Vector3f newDir = calcFacingDirection(rotation);
        facingDirection.x = newDir.x;
        facingDirection.y = newDir.y;
        facingDirection.z = newDir.z;

    }

    public void moveRotation(float offsetX, float offsetY, float offsetZ) {
        rotation.x += offsetX;
        rotation.y += offsetY;
        rotation.z += offsetZ;

        Vector3f newDir = calcFacingDirection(rotation);
        facingDirection.x = newDir.x;
        facingDirection.y = newDir.y;
        facingDirection.z = newDir.z;
    }

    /**
     * A getter
     * @return a vector that points from the location of this camera to the location at the centre of the screen
     */
    public Vector3f getFacingDirection() {
        return facingDirection;
    }

    /**
     * Static tool to calculate the camera's facing direction based on its rotation (has possibly other uses too)
     * @param rotation the object's rotation. The "front face" is assumed to be towards positive z-axis
     * @return the camera facing direction vector
     */
    public static Vector3f calcFacingDirection(Vector3f rotation) {
        float x = (float) (Math.sin(Math.toRadians(rotation.y)) * Math.cos(Math.toRadians(rotation.x)));
        float z = (float) (-Math.cos(Math.toRadians(rotation.y)) * Math.cos(Math.toRadians(rotation.x)));

        float y = -(float) Math.sin(Math.toRadians(rotation.x));

        return new Vector3f(x, y, z);
    }
}
