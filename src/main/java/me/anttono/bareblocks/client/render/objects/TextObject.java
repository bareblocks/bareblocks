package me.anttono.bareblocks.client.render.objects;

import me.anttono.bareblocks.client.render.Material;
import me.anttono.bareblocks.client.render.Mesh;
import me.anttono.bareblocks.client.render.hud.FontTexture;
import me.anttono.bareblocks.utils.FileUtils;

import java.util.ArrayList;
import java.util.List;

public class TextObject extends RenderObject {
    private static final float ZPOS = 0.0f;
    private static final int VERTICES_PER_QUAD = 4;

    private String text;

    private float width;
    private float height;

    private final FontTexture fontTexture;

    public TextObject(String text, FontTexture fontTexture) {
        super();

        this.text = text;
        this.fontTexture = fontTexture;

        this.height = fontTexture.getHeight();

        this.setMesh(buildMesh());
    }

    private Mesh buildMesh() {
        char[] characters = text.toCharArray();
        int numChars = characters.length;

        List<Float> positions = new ArrayList<>();
        List<Float> textCoords = new ArrayList<>();
        float[] normals = new float[0];
        List<Integer> indices = new ArrayList<>();

        float startx = 0f;

        for (int i = 0; i < numChars; i++) {
            FontTexture.CharInfo charInfo = fontTexture.getCharInfo(characters[i]);

            // Build a character tile out of two triangles

            // Left top corner
            positions.add(startx); //x
            positions.add(0f); //y
            positions.add(ZPOS); //z
            textCoords.add((float) charInfo.getStartX() / (float) fontTexture.getWidth());
            textCoords.add(0f);
            indices.add(i * VERTICES_PER_QUAD); // 1st triangle (reused for 2nd)

            // Left bottom corner
            positions.add(startx); //x
            positions.add((float) fontTexture.getHeight()); //y
            positions.add(ZPOS); //z
            textCoords.add((float) charInfo.getStartX() / (float) fontTexture.getWidth());
            textCoords.add(1f);
            indices.add(i * VERTICES_PER_QUAD + 1); // 1st triangle

            // Right bottom corner
            positions.add(startx + charInfo.getWidth()); //x
            positions.add((float) fontTexture.getHeight()); //y
            positions.add(ZPOS); //z
            textCoords.add((float) (charInfo.getStartX() + charInfo.getWidth()) / (float) fontTexture.getWidth());
            textCoords.add(1f);
            indices.add(i * VERTICES_PER_QUAD + 2); // 1st triangle (reused for 2nd)

            // Right top corner
            positions.add(startx + charInfo.getWidth()); //x
            positions.add(0f); //y
            positions.add(ZPOS); //z
            textCoords.add((float) (charInfo.getStartX() + charInfo.getWidth()) / (float) fontTexture.getWidth());
            textCoords.add(0f);
            indices.add(i * VERTICES_PER_QUAD + 3); // 2nd triangle

            // Add two more indices for left top and right bottom corners to finish the second triangle
            indices.add(i * VERTICES_PER_QUAD); // 2nd triangle
            indices.add(i * VERTICES_PER_QUAD + 2); // 2nd triangle

            // Move onward by the width of this character to write next character
            startx += charInfo.getWidth();
        }

        this.width = startx;

        Mesh mesh = new Mesh(FileUtils.listToArray(positions), FileUtils.listToArray(textCoords), normals, indices.stream().mapToInt(i -> i).toArray());
        mesh.setMaterial(new Material(fontTexture.getTexture()));
        return mesh;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        this.getMesh().deleteBuffers();
        this.setMesh(buildMesh());
    }
}
