package me.anttono.bareblocks.client.render.hud;

import me.anttono.bareblocks.client.render.hud.objects.IHudObject;
import me.anttono.bareblocks.client.render.objects.RenderObject;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Hud {
    private static final Font FONT = new Font("Arial", Font.PLAIN, 20);
    private static final String CHARSET = "ISO-8859-1";

    private final List<IHudObject> hudObjects;

    private final FontTexture fontTexture;

    public Hud() throws Exception {
        this.hudObjects = new ArrayList<>();

        fontTexture = new FontTexture(FONT, CHARSET);
    }

    public FontTexture getFontTexture() {
        return fontTexture;
    }

    public void addObject(IHudObject hudObject) {
        this.hudObjects.add(hudObject);
    }

    public void updateSize() {
        // Call this method for all HudObjects
        for (IHudObject object : hudObjects) {
            object.updatePos();
        }
    }

    public List<RenderObject> getRenderObjects() {
        // Call this method for all HudObjects and collect the returned lists into one list to be returned
        List<RenderObject> renderObjects = new ArrayList<>();

        for (IHudObject hudObject : hudObjects) {
            renderObjects.addAll(hudObject.getRenderObjects());
        }

        return renderObjects;
    }

    public void cleanup() {
        // Call this method for all HudObjects
        for (IHudObject hudObject : hudObjects) {
            hudObject.cleanup();
        }
    }
}
