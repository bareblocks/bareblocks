package me.anttono.bareblocks.client;

import me.anttono.bareblocks.api.types.BlockPos;
import me.anttono.bareblocks.client.io.GameInput;
import me.anttono.bareblocks.client.io.Window;
import me.anttono.bareblocks.client.render.Camera;
import me.anttono.bareblocks.client.render.Material;
import me.anttono.bareblocks.client.render.Renderer;
import me.anttono.bareblocks.client.render.Texture;
import me.anttono.bareblocks.client.render.hud.FontTexture;
import me.anttono.bareblocks.client.render.hud.Hud;
import me.anttono.bareblocks.client.render.hud.objects.HudObject;
import me.anttono.bareblocks.client.render.lights.DirectionalLight;
import me.anttono.bareblocks.client.render.lights.PointLight;
import me.anttono.bareblocks.client.render.lights.SceneLight;
import me.anttono.bareblocks.client.render.lights.SpotLight;
import me.anttono.bareblocks.client.render.objects.TextObject;
import me.anttono.bareblocks.client.world.RenderChunk;
import me.anttono.bareblocks.client.world.World;
import me.anttono.bareblocks.modloader.ModLoader;
import me.anttono.bareblocks.modloader.ModContent;
import me.anttono.bareblocks.server.GameServer;
import me.anttono.bareblocks.templates.BlockTemplate;
import me.anttono.bareblocks.templates.Mod;
import me.anttono.bareblocks.templates.WorldGeneratorTemplate;
import me.anttono.bareblocks.utils.HudUtils;
import me.anttono.bareblocks.utils.threading.EventRequestChat;
import me.anttono.bareblocks.utils.threading.events.BlockSetEvent;
import me.anttono.bareblocks.utils.threading.events.ChunkLoadEvent;
import me.anttono.bareblocks.utils.threading.events.Event;
import me.anttono.bareblocks.utils.threading.requests.BlockBreakRequest;
import me.anttono.bareblocks.utils.threading.requests.BlockPlaceRequest;
import me.anttono.bareblocks.utils.threading.requests.Request;
import me.anttono.bareblocks.utils.threading.requests.RequestType;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.glfw.GLFW.*;

public class Game {
    private static final float CAMERA_POS_STEP = 0.05f;
    private static final float MOUSE_SENSITIVITY = 0.05f;

    private final EventRequestChat eventRequestChat;

    // Camera
    private final Camera camera;
    private final Vector3f cameraInc;
    // Input
    private final GameInput gameInput;
    private final GameInput.Input forwardInput;
    private final GameInput.Input backwardInput;
    private final GameInput.Input rightInput;
    private final GameInput.Input leftInput;
    private final GameInput.Input upInput;
    private final GameInput.Input downInput;
    private final GameInput.Input lampInput;
    private final GameInput.Input spotlightInput;
    private final GameInput.Input hitInput;
    private final GameInput.Input interactInput;
    // Hud stuff
    private final Map<String, Hud> hudMap;
    private String currentHud;
    // Objects in game hud
    private TextObject upsTextObject;
    private TextObject fpsTextObject;
    private TextObject posTextObject;

    // Data from mods
    private final Mod[] mods;
    private final BlockTemplate[] blockTemplates;
    private final ContentLoader contentLoader;
    private Material textureAtlasMaterial;
    private Vector2f atlasTextureSize;


    // World related variables
    private World world;
    private int tick;
    // Lights
    private SceneLight sceneLight;
    private float lightAngle; // Directional light's angle

    // Window argument is temporary. To be removed in UI update
    public Game(GameInput gameInput, Window window) throws Exception {
        /*
        Load mods
         */
        ModLoader loader = new ModLoader();
        ModContent modContent = loader.load();

        this.mods = modContent.getMods();
        this.blockTemplates = modContent.getBlocks().toArray(new BlockTemplate[0]);
        this.contentLoader = new ContentLoader(mods);

        /*
        Prepare server
         */
        this.eventRequestChat = startServer(modContent.getWorldGenerators().toArray(new WorldGeneratorTemplate[0]));

        /*
        Prepare local game
         */
        // Camera
        this.camera = new Camera();
        this.cameraInc = new Vector3f();

        // Create inputs
        this.gameInput = gameInput;
        this.forwardInput = this.gameInput.newInput(GLFW_KEY_W, false);
        this.backwardInput = this.gameInput.newInput(GLFW_KEY_S, false);
        this.rightInput = this.gameInput.newInput(GLFW_KEY_D, false);
        this.leftInput = this.gameInput.newInput(GLFW_KEY_A, false);
        this.upInput = this.gameInput.newInput(GLFW_KEY_SPACE, false);
        this.downInput = this.gameInput.newInput(GLFW_KEY_LEFT_CONTROL, false);
        this.lampInput = this.gameInput.newInput(GLFW_KEY_L, false);
        this.spotlightInput = this.gameInput.newInput(GLFW_KEY_P, false);
        this.hitInput = this.gameInput.newInput(GLFW_MOUSE_BUTTON_1, true);
        this.interactInput = this.gameInput.newInput(GLFW_MOUSE_BUTTON_2, true);

        // Load all textures from mods and create a chunk mesh
        Texture textureAtlas = contentLoader.loadTextures();
        this.textureAtlasMaterial = new Material(textureAtlas);
        this.atlasTextureSize = new Vector2f(
                (float) ContentLoader.TEXTURE_SIZE / textureAtlas.getWidth(),
                (float) ContentLoader.TEXTURE_SIZE / textureAtlas.getHeight()
        );
        // Generate BlockTextures
        for (BlockTemplate template : blockTemplates) {
            template.constructTexture(mods, contentLoader.getTextureIds());
        }

        // Create a world
        this.world = new World(eventRequestChat, 6);
        this.camera.setPosition(1, 12, 1);
        world.setCameraWorldPos(camera.getPosition(), false);

        this.tick = 0;

        // Create sceneLight and set all it's data
        sceneLight = new SceneLight();

        // Ambient light
        sceneLight.setAmbientLight(new Vector3f(0.3f, 0.3f, 0.3f));

        // Point light
        Vector3f lightColour = new Vector3f(1, 1, 1);
        Vector3f lightPosition = new Vector3f(1, 0, -2);
        float lightIntensity = 1.0f;
        PointLight[] pointLights = new PointLight[]{new PointLight(lightColour, lightPosition, lightIntensity)};
        PointLight.Attenuation att = new PointLight.Attenuation(0.0f, 0.0f, 0.2f);
        pointLights[0].setAttenuation(att);
        sceneLight.setPointLights(pointLights);

        lightPosition = new Vector3f(0f, 0f, 10f);
        PointLight slPointLight = new PointLight(new Vector3f(1, 1, 1), lightPosition, lightIntensity);
        att = new PointLight.Attenuation(0f, 0f, 0.02f);
        slPointLight.setAttenuation(att);
        Vector3f coneDir = new Vector3f(0, 0, -1);
        float cutOff = (float) Math.cos(Math.toRadians(5));
        SpotLight[] spotLights = new SpotLight[]{new SpotLight(slPointLight, coneDir, cutOff)};
        sceneLight.setSpotLights(spotLights);

        // Directional light
        lightPosition = new Vector3f(1, 1, 1);
        sceneLight.setDirectionalLight(new DirectionalLight(lightPosition, new Vector3f(1, 1, 1), lightIntensity));

        this.lightAngle = -90;

        // Create main HUD for the game
        this.hudMap = new HashMap<>();
        hudMap.put("game", createGameHud(window));
        currentHud = "game";
    }

    private EventRequestChat startServer(WorldGeneratorTemplate[] worldGeneratorTemplates) {
        EventRequestChat chat = new EventRequestChat();

        GameServer server = new GameServer(chat, worldGeneratorTemplates);
        server.start();

        return chat;
    }

    public void input() {
        // Movement
        cameraInc.set(0, 0, 0);
        if (forwardInput.isActive()) {
            cameraInc.z = -1;
        } else if (backwardInput.isActive()) {
            cameraInc.z = 1;
        }
        if (leftInput.isActive()) {
            cameraInc.x = -1;
        } else if (rightInput.isActive()) {
            cameraInc.x = 1;
        }
        if (upInput.isActive()) {
            cameraInc.y = 1;
        } else if (downInput.isActive()) {
            cameraInc.y = -1;
        }
        // Set PointLight position
        if (lampInput.isActive()) {
            sceneLight.getPointLights()[0].getPosition().x = camera.getPosition().x;
            sceneLight.getPointLights()[0].getPosition().y = camera.getPosition().y;
            sceneLight.getPointLights()[0].getPosition().z = camera.getPosition().z;
        }
        // Set SpotLight position and direction
        if (spotlightInput.isActive()) {
            sceneLight.getSpotLights()[0].getPointLight().getPosition().x = camera.getPosition().x;
            sceneLight.getSpotLights()[0].getPointLight().getPosition().y = camera.getPosition().y;
            sceneLight.getSpotLights()[0].getPointLight().getPosition().z = camera.getPosition().z;
            sceneLight.getSpotLights()[0].getConeDirection().x = camera.getFacingDirection().x;
            sceneLight.getSpotLights()[0].getConeDirection().y = camera.getFacingDirection().y;
            sceneLight.getSpotLights()[0].getConeDirection().z = camera.getFacingDirection().z;
        }

        // Mouse buttons
        if (hitInput.isJustPressed()) {
            // Request to break the targeted block
            this.world.getTargetedBlockPos(
                    this.camera.getPosition(),
                    this.camera.getFacingDirection(),
                    World.MAX_INTERACT_DISTANCE
            ).ifPresent((pos) -> eventRequestChat.addRequest(new BlockBreakRequest(this.tick, pos)));
        }
        if (interactInput.isJustPressed()) {
            // Request to place a copy of the targeted block to the targeted free position
            int targetBlockType = this.world.getTargetedBlockPos(
                    this.camera.getPosition(),
                    this.camera.getFacingDirection(),
                    World.MAX_INTERACT_DISTANCE
            ).map(pos -> this.world.getBlockAt(pos)).orElse(0);
            this.world.getTargetedEmptyBlockPos(
                    this.camera.getPosition(),
                    this.camera.getFacingDirection(),
                    World.MAX_INTERACT_DISTANCE
            ).ifPresent((pos) -> eventRequestChat.addRequest(new BlockPlaceRequest(this.tick, pos, targetBlockType)));
        }
    }

    public void update(int fps, int ups) {
        // Count ticks
        this.tick++;

        // View some debug information (will be improved in UI update)
        upsTextObject.setText("UPS: " + ups);
        fpsTextObject.setText("FPS: " + fps);
        posTextObject.setText("Pos: " + String.format("%.2f", camera.getPosition().x) + ", " + String.format("%.2f", camera.getPosition().y) + ", " + String.format("%.2f", camera.getPosition().z));

        // Update directional light
        lightAngle += 1.1f;
        if (lightAngle > 90) {
            sceneLight.getDirectionalLight().setIntensity(0);
            if (lightAngle >= 270) {
                lightAngle = -90;
            }
        } else if (lightAngle <= -80 || lightAngle >= 80) {
            float factor = 1 - (Math.abs(lightAngle) - 80) / 10.0f;
            sceneLight.getDirectionalLight().setIntensity(factor);
            sceneLight.getDirectionalLight().getColour().y = Math.max(factor, 0.9f);
            sceneLight.getDirectionalLight().getColour().z = Math.max(factor, 0.5f);
        } else {
            sceneLight.getDirectionalLight().setIntensity(1);
            sceneLight.getDirectionalLight().getColour().x = 1;
            sceneLight.getDirectionalLight().getColour().y = 1;
            sceneLight.getDirectionalLight().getColour().z = 1;
        }
        double angRad = Math.toRadians(lightAngle);
        sceneLight.getDirectionalLight().getDirection().x = (float) Math.sin(angRad);
        sceneLight.getDirectionalLight().getDirection().y = (float) Math.cos(angRad);

        // Handle upcoming events
        Event event = eventRequestChat.getEvent();
        while (event != null) {
            switch (event.getEventType()) {
                case PING_EVENT: {
                    System.out.println(event.getSentTick() + " " + event.getTick() + " " + tick + " ping: " + (tick - event.getSentTick()));
                    break;
                }
                case CHUNK_LOAD_EVENT: {
                    ChunkLoadEvent chunkLoadEvent = (ChunkLoadEvent) event;
                    System.out.println("CLIENT: chunkLoadEvent (" + chunkLoadEvent.getPos().x + ", " + chunkLoadEvent.getPos().y + ", " + chunkLoadEvent.getPos().z + ")");
                    RenderChunk chunk = new RenderChunk(world, chunkLoadEvent.getPos(), chunkLoadEvent.getChunkData(), blockTemplates, atlasTextureSize);
                    world.setChunk(chunkLoadEvent.getPos(), chunk);
                    break;
                }
                case BLOCK_SET_EVENT: {
                    BlockSetEvent blockSetEvent = (BlockSetEvent) event;
                    BlockPos blockPos = blockSetEvent.getPos();
                    int blockType = blockSetEvent.getBlockType();
                    world.setBlock(blockPos, blockType);
                    break;
                }
            }
            // Get the next event
            event = eventRequestChat.getEvent();
        }

        // Update camera position
        camera.movePosition(cameraInc.x * CAMERA_POS_STEP, cameraInc.y * CAMERA_POS_STEP, cameraInc.z * CAMERA_POS_STEP);
        world.setCameraWorldPos(camera.getPosition(), false);

        // Update camera rotation
        if (gameInput.isCursorHidden()) {
            Vector2f rotVec = gameInput.getMouseDisplVec();
            camera.moveRotation(rotVec.x * MOUSE_SENSITIVITY, rotVec.y * MOUSE_SENSITIVITY, 0);
        }
    }

    public void render(Renderer renderer, Window window) {
        hudMap.get(currentHud).updateSize();

        renderer.render(window, camera, world, sceneLight, textureAtlasMaterial);
        renderer.renderHud(window, hudMap.get(currentHud));
    }

    /**
     * Exit the server, stop the currently running game and clean up memory.
     */
    public void stop() {
        eventRequestChat.addRequest(new Request(tick, RequestType.STOP_REQUEST));

        textureAtlasMaterial.getTexture().ifPresent(Texture::cleanup);

        // Cleanup for every hud
        for (Map.Entry<String, Hud> entry : hudMap.entrySet()) {
            entry.getValue().cleanup();
        }

        world.cleanup();
    }

    // Temporary, will be removed in UI update
    private Hud createGameHud(Window window) throws Exception {
        Hud gameHud = new Hud();

        FontTexture fontTexture = gameHud.getFontTexture();

        // Display current ups
        upsTextObject = new TextObject("UPS: ", fontTexture);
        upsTextObject.getMesh().getMaterial().setAmbientColour(new Vector4f(1, 1, 1, 1));
        HudObject upsObject = new HudObject(upsTextObject, upsTextObject.getWidth(), upsTextObject.getHeight());
        upsObject.setPositionAnchor(window.getTopLeftAnchor());
        upsObject.setLayer(0);
        gameHud.addObject(upsObject);

        // Display current fps
        fpsTextObject = new TextObject("FPS: ", fontTexture);
        fpsTextObject.getMesh().getMaterial().setAmbientColour(new Vector4f(1, 1, 1, 1));
        HudObject fpsObject = new HudObject(fpsTextObject, fpsTextObject.getWidth(), fpsTextObject.getHeight());
        fpsObject.setPositionAnchor(upsObject.getBottomLeftAnchor());
        fpsObject.setLayer(0);
        gameHud.addObject(fpsObject);

        // Display current position
        posTextObject = new TextObject("Pos: ", fontTexture);
        posTextObject.getMesh().getMaterial().setAmbientColour(new Vector4f(1, 1, 1, 1));
        HudObject posObject = new HudObject(posTextObject, posTextObject.getWidth(), posTextObject.getHeight());
        posObject.setPositionAnchor(fpsObject.getBottomLeftAnchor());
        posObject.setLayer(0);
        gameHud.addObject(posObject);

        // Cross in the middle of the screen
        Texture crossTexture = new Texture("textures/default-cross.png");
        HudObject crossObject = HudUtils.generateHudRectangle(40f, 40f, new Vector4f(0.8f, 0.8f, 0.8f, 0.5f), crossTexture);
        crossObject.setPositionAnchor(window.getCentreAnchor());
        crossObject.setPositionShift(-20f, -20f);
        crossObject.setLayer(0);
        gameHud.addObject(crossObject);

        return gameHud;
    }
}
