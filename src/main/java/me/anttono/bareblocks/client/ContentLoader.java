package me.anttono.bareblocks.client;

import me.anttono.bareblocks.client.render.Mesh;
import me.anttono.bareblocks.client.render.ShaderProgram;
import me.anttono.bareblocks.client.render.Texture;
import me.anttono.bareblocks.templates.Mod;
import me.anttono.bareblocks.utils.FileUtils;
import org.joml.Vector3f;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL32.glFramebufferTexture;

public class ContentLoader {
    public static final int TEXTURE_SIZE = 128;
    public static final int TEXTURES_PER_ROW = 8;
    private static final float[] facePositions = {
            0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
    };
    private static final float[] faceTextCoords = {
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
    };
    private static final float[] faceNormals = {
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
    };
    private static final int[] faceIndices = {
            0, 2, 1,
            3, 1, 2,
    };

    private final Mod[] mods;
    private final Map<String, Integer> textureIds;

    public ContentLoader(Mod[] mods) {
        this.mods = mods;
        this.textureIds = new HashMap<>();
    }

    public Texture loadTextures() throws Exception {
        List<String> texturePaths = new ArrayList<>();
        for (Mod mod : mods) {
            loadTextureFolder(mod.getTextureFolderPath(), texturePaths);
        }
        return buildTextureAtlas(texturePaths);
    }

    private void loadTextureFolder(String folderPath, List<String> texturePathCollection) throws Exception {
        File textureFolder = new File(folderPath);
        File[] folderContent = textureFolder.listFiles();
        if (folderContent == null)
            throw new Exception("Failed to load textures: could not read content of the texture folder'" + folderPath + "'.");
        for (File file : folderContent) {
            Path filePath = file.toPath();
            if (!Files.exists(filePath))
                throw new Exception("Failed to load textures: could not read texture file '" + filePath + "'.");
            if (Files.isDirectory(filePath)) {
                loadTextureFolder(filePath.toString(), texturePathCollection);
            } else {
                texturePathCollection.add(filePath.toString());
                String globalName = filePath.toString().split("\\.")[0];
                textureIds.put(globalName, textureIds.size());
            }
        }
    }

    private Texture buildTextureAtlas(List<String> texturePaths) throws Exception {
        // Texture atlas dimensions
        int width = TEXTURE_SIZE * TEXTURES_PER_ROW;
        int height = TEXTURE_SIZE * (texturePaths.size() % TEXTURES_PER_ROW == 0 ? texturePaths.size() / TEXTURES_PER_ROW : texturePaths.size() / TEXTURES_PER_ROW + 1);
        float heightPerTexture = (float) TEXTURE_SIZE / height * 2;

        // Prepare a shaderProgram
        ShaderProgram atlasShaderProgram = new ShaderProgram();
        atlasShaderProgram.createVertexShader(FileUtils.loadResource("/shaders/atlas_vertex.vs"));
        atlasShaderProgram.createFragmentShader(FileUtils.loadResource("/shaders/atlas_fragment.fs"));
        atlasShaderProgram.link();
        atlasShaderProgram.createUniform("textureSampler");
        atlasShaderProgram.createUniform("scale");
        atlasShaderProgram.createUniform("offset");

        // Prepare a mesh to use for rendering the textures
        Mesh textureMesh = new Mesh(facePositions, faceTextCoords, faceNormals, faceIndices);

        // Create a framebuffer to act as a render target for rendering the texture atlas
        int frameBufferId = glGenFramebuffers();
        glBindFramebuffer(GL_FRAMEBUFFER, frameBufferId);

        // Create the texture to render to
        int textureAtlasId = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, textureAtlasId);
        // Initialize the texture to be empty
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
        // Use pixelated filtering
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        // Configure framebuffer
        glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, textureAtlasId, 0);
        glDrawBuffers(new int[]{GL_COLOR_ATTACHMENT0});

        // Throw an exception if something went wrong in the process
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            throw new Exception("Failed to create a framebuffer for texture atlas");

        // Render to the framebuffer
        glBindFramebuffer(GL_FRAMEBUFFER, frameBufferId);
        glViewport(0, 0, width, height);

        // Render the textures to the atlas
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        atlasShaderProgram.bind();
        atlasShaderProgram.setUniform("scale", new Vector3f(0.25f, heightPerTexture, 1f));
        atlasShaderProgram.setUniform("textureSampler", 0);
        for (int i = 0; i < texturePaths.size(); i++) {
            // Load the texture
            Texture texture = new Texture(texturePaths.get(i));
            // Init render
            atlasShaderProgram.setUniform("offset", new Vector3f(-1f + 0.25f * (i % TEXTURES_PER_ROW), -1f + heightPerTexture * (i / TEXTURES_PER_ROW), 0f));
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, texture.getId());
            glBindVertexArray(textureMesh.getVaoId());
            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);
            // Draw call
            glDrawElements(GL_TRIANGLES, textureMesh.getVertexCount(), GL_UNSIGNED_INT, 0);
            // Cleanup
            glDisableVertexAttribArray(0);
            glDisableVertexAttribArray(1);
            glBindVertexArray(0);
            glBindTexture(GL_TEXTURE_2D, 0);
            texture.cleanup();
        }

        // Cleanup
        atlasShaderProgram.unbind();
        atlasShaderProgram.cleanup();
        textureMesh.cleanup();
        glDeleteFramebuffers(frameBufferId);

        return new Texture(textureAtlasId, width, height);
    }

    public Map<String, Integer> getTextureIds() {
        return textureIds;
    }
}
