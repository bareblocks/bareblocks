package me.anttono.bareblocks.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ErrorLogger {
    // Static, so they can be all saved with one call
    private static final Map<String, List<String>> logs = new HashMap<>();

    // Name of this log, saved to logs with this name
    private final String name;

    public ErrorLogger(String name) {
        this.name = name;
        logs.put(name, new ArrayList<>());
    }

    // Adds given string to the end of this log as new line
    public void addLine(String line) {
        logs.get(name).add(line);
    }

    public void addWarning(String warning) {
        this.addLine("[Warning] " + warning);
    }

    public void addError(String error) throws Exception {
        this.addLine("[Error] " + error);
        throw new Exception("Error occurred. Check \"" + name + "\" from logs for more info.");
    }

    public void addDebugMessage(String message) {
        this.addLine("[Debug] " + message);
    }

    public void addDebugMessage(int data) {
        this.addLine("[Debug] " + ((Integer) data));
    }

    public void addDebugMessage(float data) {
        this.addLine("[Debug] " + ((Float) data));
    }

    public static void save() {
        // Create folder for logs
        File folder = new File("logs");
        folder.mkdirs(); // The output can be ignored because it is true always after the folder has been created for the first time

        for (Map.Entry<String, List<String>> entry : logs.entrySet()) {
            String filename = "logs/" + entry.getKey() + ".txt";
            try {
                // Try to create log file for writing
                FileWriter writer = new FileWriter(filename);

                // Write all lines to the file
                for (String line : entry.getValue()) {
                    writer.write(line + '\n');
                }

                // Close the file when done
                writer.close();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }
}
