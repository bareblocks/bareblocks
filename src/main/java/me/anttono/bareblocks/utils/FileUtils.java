package me.anttono.bareblocks.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileUtils {
    /**
     * Reads the content of the given file from resources folder into a single string
     * @param fileName path to the file in resources folder. Must start with a slash
     * @return The content of the file as a single string
     * @throws Exception if the file reading fails for any reason
     */
    public static String loadResource(String fileName) throws Exception {
        String result;
        try (
                InputStream in = FileUtils.class.getResourceAsStream(fileName);
                Scanner scanner = new Scanner(in, java.nio.charset.StandardCharsets.UTF_8.name())
        ) {
            result = scanner.useDelimiter("\\A").next();
        }
        return result;
    }

    public static List<String> readAllLines(String fileName) throws Exception {
        // The resulting list of lines
        List<String> list = new ArrayList<>();

        // Turn the provided fileName into a file
        File file = new File(fileName);

        // Use BufferedReader to read the file
        BufferedReader reader = new BufferedReader(new FileReader(file));

        // Read every line to list
        String line;
        while ((line = reader.readLine()) != null) {
            list.add(line);
        }

        return list;
    }

    public static float[] listToArray(List<Float> list) {
        int size = list != null ? list.size() : 0;
        float[] floatArr = new float[size];
        for (int i = 0; i < size; i++) {
            floatArr[i] = list.get(i);
        }
        return floatArr;
    }
}
