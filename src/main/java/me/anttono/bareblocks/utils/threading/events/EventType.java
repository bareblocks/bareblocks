package me.anttono.bareblocks.utils.threading.events;

public enum EventType {
    PING_EVENT,
    CHUNK_LOAD_EVENT,
    BLOCK_SET_EVENT
}
