package me.anttono.bareblocks.utils.threading.requests;

import me.anttono.bareblocks.api.types.BlockPos;

public class BlockBreakRequest extends Request {
    private final BlockPos pos;

    public BlockBreakRequest(int tick, BlockPos pos) {
        super(tick, RequestType.BLOCK_BREAK_REQUEST);
        this.pos = pos;
    }

    public BlockPos getPos() {
        return pos;
    }
}
