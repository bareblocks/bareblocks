package me.anttono.bareblocks.utils.threading.requests;

import me.anttono.bareblocks.api.types.ChunkPos;

public class ChunkLoadRequest extends Request {
    private final ChunkPos pos;

    public ChunkLoadRequest(int tick, RequestType requestType, ChunkPos pos) {
        super(tick, requestType);
        this.pos = pos;
    }

    public ChunkLoadRequest(int tick, ChunkPos pos) {
        this(tick, RequestType.CHUNK_LOAD_REQUEST, pos);
    }

    public ChunkPos getPos() {
        return pos;
    }
}
