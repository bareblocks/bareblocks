package me.anttono.bareblocks.utils.threading.requests;

public enum RequestType {
    PING_REQUEST,
    STOP_REQUEST,
    CHUNK_LOAD_REQUEST,
    BLOCK_BREAK_REQUEST,
    BLOCK_PLACE_REQUEST
}
