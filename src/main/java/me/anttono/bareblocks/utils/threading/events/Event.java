package me.anttono.bareblocks.utils.threading.events;

public class Event {
    private final int sentTick;
    private final int tick;
    private final EventType eventType;

    public Event(int tick, int sentTick, EventType eventType) {
        this.tick = tick;
        this.sentTick = sentTick;
        this.eventType = eventType;
    }

    public int getTick() {
        return tick;
    }

    public int getSentTick() {
        return sentTick;
    }

    public EventType getEventType() {
        return eventType;
    }
}
