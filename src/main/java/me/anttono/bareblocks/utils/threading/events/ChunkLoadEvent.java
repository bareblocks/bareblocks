package me.anttono.bareblocks.utils.threading.events;

import me.anttono.bareblocks.api.types.ChunkPos;

public class ChunkLoadEvent extends Event {
    private final int[][][] chunkData;
    private final ChunkPos pos;

    public ChunkLoadEvent(int tick, int sentTick, EventType eventType, ChunkPos pos, int[][][] chunkData) {
        super(tick, sentTick, eventType);
        this.chunkData = chunkData;
        this.pos = pos;
    }

    public int[][][] getChunkData() {
        return chunkData;
    }

    public ChunkPos getPos() {
        return pos;
    }
}
