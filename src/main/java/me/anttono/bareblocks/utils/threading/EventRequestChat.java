package me.anttono.bareblocks.utils.threading;

import me.anttono.bareblocks.utils.threading.events.Event;
import me.anttono.bareblocks.utils.threading.requests.Request;

import java.util.Deque;
import java.util.LinkedList;

public class EventRequestChat {
    private final Deque<Request> requests = new LinkedList<>();
    private final Deque<Event> events = new LinkedList<>();

    public void addRequest(Request request) {
        requests.addLast(request);
    }

    public Request getRequest() {
        if (requests.isEmpty()) {
            return null;
        } else {
            return requests.removeFirst();
        }
    }

    public void addEvent(Event event) {
        events.addLast(event);
    }

    public Event getEvent() {
        if (events.isEmpty()) {
            return null;
        } else {
            return events.removeFirst();
        }
    }

    public synchronized void serverWait() {
        try {
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void serverStart() {
        notify();
    }
}
