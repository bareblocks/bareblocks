package me.anttono.bareblocks.utils.threading.requests;

public class Request {
    private final int tick;
    private final RequestType requestType;

    public Request(int tick, RequestType requestType) {
        this.tick = tick;
        this.requestType = requestType;
    }

    public int getTick() {
        return tick;
    }

    public RequestType getRequestType() {
        return requestType;
    }
}
