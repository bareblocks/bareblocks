package me.anttono.bareblocks.utils.threading.requests;

import me.anttono.bareblocks.api.types.BlockPos;

public class BlockPlaceRequest extends Request {
    private final BlockPos pos;
    private final int blockType;

    public BlockPlaceRequest(int tick, BlockPos pos, int blockType) {
        super(tick, RequestType.BLOCK_PLACE_REQUEST);
        this.pos = pos;
        this.blockType = blockType;
    }

    public BlockPos getPos() {
        return pos;
    }

    public int getBlockType() {
        return blockType;
    }
}
