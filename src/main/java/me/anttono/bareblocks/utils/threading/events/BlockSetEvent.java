package me.anttono.bareblocks.utils.threading.events;

import me.anttono.bareblocks.api.types.BlockPos;

public class BlockSetEvent extends Event {
    private final BlockPos pos;
    private final int blockType;

    public BlockSetEvent(int tick, int sentTick, BlockPos pos, int blockType) {
        super(tick, sentTick, EventType.BLOCK_SET_EVENT);
        this.pos = pos;
        this.blockType = blockType;
    }

    public BlockPos getPos() {
        return pos;
    }

    public int getBlockType() {
        return blockType;
    }
}
