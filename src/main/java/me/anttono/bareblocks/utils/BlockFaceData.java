package me.anttono.bareblocks.utils;

import java.util.Arrays;
import java.util.List;

public class BlockFaceData {
    public static class Up {
        public static final List<Float> POSITIONS = Arrays.asList(
                0f, 1f, 0f,
                1f, 1f, 0f,
                0f, 1f, 1f,
                1f, 1f, 1f
        );

        public static final List<Float> NORMALS = Arrays.asList(
                0f, 1f, 0f,
                0f, 1f, 0f,
                0f, 1f, 0f,
                0f, 1f, 0f
        );
    }

    public static class Down {
        public static final List<Float> POSITIONS = Arrays.asList(
                1f, 0f, 0f,
                0f, 0f, 0f,
                1f, 0f, 1f,
                0f, 0f, 1f
        );

        public static final List<Float> NORMALS = Arrays.asList(
                0f, -1f, 0f,
                0f, -1f, 0f,
                0f, -1f, 0f,
                0f, -1f, 0f
        );
    }

    public static class North {
        public static final List<Float> POSITIONS = Arrays.asList(
                1f, 0f, 1f,
                1f, 1f, 1f,
                1f, 0f, 0f,
                1f, 1f, 0f
        );

        public static final List<Float> NORMALS = Arrays.asList(
                1f, 0f, 0f,
                1f, 0f, 0f,
                1f, 0f, 0f,
                1f, 0f, 0f
        );
    }

    public static class South {
        public static final List<Float> POSITIONS = Arrays.asList(
                0f, 0f, 0f,
                0f, 1f, 0f,
                0f, 0f, 1f,
                0f, 1f, 1f
        );

        public static final List<Float> NORMALS = Arrays.asList(
                -1f, 0f, 0f,
                -1f, 0f, 0f,
                -1f, 0f, 0f,
                -1f, 0f, 0f
        );
    }

    public static class East {
        public static final List<Float> POSITIONS = Arrays.asList(
                0f, 0f, 1f,
                0f, 1f, 1f,
                1f, 0f, 1f,
                1f, 1f, 1f
        );

        public static final List<Float> NORMALS = Arrays.asList(
                0f, 0f, 1f,
                0f, 0f, 1f,
                0f, 0f, 1f,
                0f, 0f, 1f
        );
    }

    public static class West {
        public static final List<Float> POSITIONS = Arrays.asList(
                1f, 0f, 0f,
                1f, 1f, 0f,
                0f, 0f, 0f,
                0f, 1f, 0f
        );

        public static final List<Float> NORMALS = Arrays.asList(
                0f, 0f, -1f,
                0f, 0f, -1f,
                0f, 0f, -1f,
                0f, 0f, -1f
        );
    }

    public static final List<Integer> INDICES = Arrays.asList(
            0, 2, 1,
            3, 1, 2
    );

    public static final List<Float> TEXTURE_COORDS = Arrays.asList(
            0f, 0f,
            0f, 1f,
            1f, 0f,
            1f, 1f
    );

    public static final float[] blockWireframePositions = new float[]{
            0f, 0f, 0f,
            1f, 0f, 0f,
            0f, 1f, 0f,
            1f, 1f, 0f,
            0f, 0f, 1f,
            1f, 0f, 1f,
            0f, 1f, 1f,
            1f, 1f, 1f,
    };
    public static final int[] blocWireframeIndices = new int[]{
            0, 1,
            0, 2,
            0, 4,
            1, 3,
            1, 5,
            2, 3,
            2, 6,
            3, 7,
            4, 5,
            4, 6,
            5, 7,
            6, 7,
    };
}
