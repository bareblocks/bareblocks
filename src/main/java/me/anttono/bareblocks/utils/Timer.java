package me.anttono.bareblocks.utils;

import java.util.Deque;
import java.util.LinkedList;

public class Timer {
    private double lastLoopTime;

    private final Deque<Double> updates = new LinkedList<>();
    private final Deque<Double> frames = new LinkedList<>();

    public void init() {
        lastLoopTime = getTime();
    }

    public double getTime() {
        return System.nanoTime() / 1_000_000_000.0;
    }

    public float getElapsedTime() {
        double time = getTime();
        float elapsedTime = (float) (time - lastLoopTime);
        lastLoopTime = time;
        return elapsedTime;
    }

    public double getLastLoopTime() {
        return lastLoopTime;
    }

    public void addUpdate() {
        // This method takes care of calculating current ups
        // It must be called exactly once every update
        updates.addLast(lastLoopTime);

        double first = updates.peek() != null ? updates.peek() : 0.0d;
        double change = lastLoopTime - first;

        while (change > 1.0d) {
            updates.removeFirst();
            first = updates.peek() != null ? updates.peek() : 0.0d;
            change = lastLoopTime - first;
        }
    }

    public int getUps() {
        return updates.size();
    }

    public void addFrame() {
        // This method takes care of calculating current fps
        // It must be called exactly once every render cycle
        frames.addLast(lastLoopTime);

        double first = frames.peek() != null ? frames.peek() : 0.0d;
        double change = lastLoopTime - first;

        while (change > 1.0d) {
            frames.removeFirst();
            first = frames.peek() != null ? frames.peek() : 0.0d;
            change = lastLoopTime - first;
        }
    }

    public int getFps() {
        return frames.size();
    }
}
