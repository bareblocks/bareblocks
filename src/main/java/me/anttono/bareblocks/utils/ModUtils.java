package me.anttono.bareblocks.utils;

import me.anttono.bareblocks.modloader.data.DataSource;
import me.anttono.bareblocks.modloader.exceptions.*;
import me.anttono.bareblocks.templates.Mod;

import java.util.List;
import java.util.Objects;

/**
 * A collection of utility functions for the mod loading process.
 */
public class ModUtils {
    /**
     * Convert the given name into a name in the global name format if it isn't already. Global name format means
     * "mod.name" where 'mod' is the name of the source mod and 'name' is the name to convert.
     * @param name the name to convert. Can be of format "name" or "mod.name".
     * @param source information about the source of the name, including the source mod.
     * @return the given name in the global name format.
     * @throws InvalidNameException if the given name contains too many dots
     */
    public static String globalName(String name, DataSource source) throws InvalidNameException {
        String[] nameParts = name.split("\\.");
        if (nameParts.length == 1) {
            return source.getSourceMod().getMod() + "." + name;
        } else if (nameParts.length == 2) {
            return name;
        } else {
            throw new InvalidNameException(source, name);
        }
    }

    /**
     * Convert the given path into a name in the global path format if it isn't already. Global path format means
     * the full path of the texture beginning from the folder of the game jar, using \ as folder separator.
     * @param mods list of loaded mods to check for mod existence.
     * @param mod the mod this texture is from.
     * @param path local path to the texture. Takes the format "mod.folder/file" where 'mod' is the name of the mod
     *             to take the texture from, 'folder' is the folder path to the texture and 'file' is the file name of
     *             the texture without extensions. 'mod' and 'folder' parts are optional.
     * @param source information about the source of the texture, including the source mod.
     * @return the given path in the global path format.
     * @throws MissingModException if the mod requested by the path isn't loaded.
     * @throws InvalidTexturePathException if the given name contains too many dots.
     */
    public static String globalTexturePath(Mod[] mods, Mod mod, String path, DataSource source) throws MissingModException, InvalidTexturePathException {
        String[] pathParts = path.split("\\.");
        if (pathParts.length == 1) {
            return mod.getTextureFolderPath() + "\\" + path.replace("/", "\\");
        } else if (pathParts.length == 2) {
            String requestedModName = pathParts[0];
            for (Mod requestedMod : mods) {
                if (Objects.equals(requestedMod.getMod(), requestedModName)) {
                    return requestedMod.getTextureFolderPath() + "\\" + pathParts[1].replace("/", "\\");
                }
            }
            throw new MissingModException(source, requestedModName);
        } else {
            throw new InvalidTexturePathException(source, path);
        }
    }

    /**
     * Require the existence of a field by checking that the given field has a non-null value. Intended for use after
     * loading objects from yml-files.
     * @param field field to check.
     * @param fieldName the name of the field to be used in potential error messages.
     * @param source information about the source of the field to be used in potential error messages.
     * @throws MissingVariableException if the value of the field is null.
     */
    public static void requireFieldExistence(Object field, String fieldName, DataSource source) throws MissingVariableException {
        if (field == null) throw new MissingVariableException(source, fieldName);
    }

    /**
     * Require the existence of a word field by checking that the given String field has a non-null value, and the
     * String contains only a single word without whitespaces. Intended for use after loading objects from yml-files.
     * @param field the String field to check.
     * @param fieldName the name of the field to be used in potential error messages.
     * @param source information about the source of the field to be used in potential error messages.
     * @throws MissingVariableException if the value of the field is null.
     * @throws InvalidWordException if the String value is non-null and contains whitespaces.
     */
    public static void requireWordFieldExistence(String field, String fieldName, DataSource source) throws MissingVariableException, InvalidWordException {
        requireFieldExistence(field, fieldName, source);
        if (field.contains(" ")) throw new InvalidWordException(source, fieldName);
    }

    /**
     * Require the String field to contain only a single word without whitespaces if it is non-null. This means that the
     * existence of the field is optional, but if it exists, it must contain a single word String. Intended for use
     * after loading objects from yml-files.
     * @param field the String field to check.
     * @param fieldName the name of the field to be used in potential error messages.
     * @param source information about the source of the field to be used in potential error messages.
     * @throws InvalidWordException if the String value is non-null and contains whitespaces.
     */
    public static void requireWordField(String field, String fieldName, DataSource source) throws InvalidWordException {
        if (field != null && field.contains(" ")) throw new InvalidWordException(source, fieldName);
    }

    /**
     * Require the List field to contain at least a given number of elements. Intended for use after loading objects
     * from yml-files.
     * @param field the List field to check
     * @param requiredCount the number of elements required.
     * @param fieldName the name of the field to be used in potential error messages.
     * @param source information about the source of the field to be used in potential error messages.
     * @throws MissingArgumentsException if the List field contains less elements than the requiredCount.
     */
    public static void requireArgumentCount(List field, int requiredCount, String fieldName, DataSource source) throws MissingArgumentsException {
        if (field.size() < requiredCount) throw new MissingArgumentsException(source, fieldName, requiredCount, field.size());
    }
}
