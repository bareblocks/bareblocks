package me.anttono.bareblocks.utils;

import me.anttono.bareblocks.client.render.Material;
import me.anttono.bareblocks.client.render.Mesh;
import me.anttono.bareblocks.client.render.Texture;
import me.anttono.bareblocks.client.render.hud.objects.HudObject;
import me.anttono.bareblocks.client.render.objects.RenderObject;
import org.joml.Vector4f;

public class HudUtils {


    public static HudObject generateHudRectangle(float width, float height, Vector4f colour, Texture texture) {
        float[] positions = new float[]{
                0f, 0f, 0f,
                0f, height, 0f,
                width, height, 0f,
                width, 0f, 0f
        };
        float[] textCoords = new float[]{
                0f, 0f,
                0f, 1f,
                1f, 1f,
                1f, 0f
        };
        float[] normals = new float[0];
        int[] indices = new int[]{0, 1, 2, 0, 2, 3};

        Mesh rectangleMesh = new Mesh(positions, textCoords, normals, indices);

        Material rectangleMaterial = texture != null ? new Material(texture) : new Material();
        rectangleMaterial.setAmbientColour(colour);
        rectangleMesh.setMaterial(rectangleMaterial);

        return new HudObject(new RenderObject(rectangleMesh), width, height);
    }
}
