package me.anttono.bareblocks.utils;

import java.util.List;

public class DataUtils {
    /**
     * Convert a list of Floats into a primitive float array.
     * @param list the list to convert
     * @return the created array
     */
    public static float[] toFloatArray(List<Float> list) {
        float[] result = new float[list.size()];
        for (int i = 0; i < list.size(); i++) {
            result[i] = list.get(i);
        }
        return result;
    }
}
