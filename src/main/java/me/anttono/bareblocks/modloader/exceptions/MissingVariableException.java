package me.anttono.bareblocks.modloader.exceptions;

import me.anttono.bareblocks.modloader.data.DataSource;

public class MissingVariableException extends ModLoaderException {
    private final String variableName;

    public MissingVariableException(DataSource source, String variableName) {
        super(source, "Mandatory variable '" + variableName + "' doesn't exist or is missing a value.");
        this.variableName = variableName;
    }

    public String getVariableName() {
        return variableName;
    }
}
