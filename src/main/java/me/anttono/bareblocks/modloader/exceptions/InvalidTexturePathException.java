package me.anttono.bareblocks.modloader.exceptions;

import me.anttono.bareblocks.modloader.data.DataSource;

public class InvalidTexturePathException extends ModLoaderException {
    private final String path;

    public InvalidTexturePathException(DataSource source, String path) {
        super(source, "Invalid texture path '" + path + "'. Path can contain only one dot to separate mod name from actual file path.");
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
