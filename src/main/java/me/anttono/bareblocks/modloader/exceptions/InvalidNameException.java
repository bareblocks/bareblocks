package me.anttono.bareblocks.modloader.exceptions;

import me.anttono.bareblocks.modloader.data.DataSource;

public class InvalidNameException extends ModLoaderException {
    private final String name;

    public InvalidNameException(DataSource source, String name) {
        super(source, "Invalid object name '" + name + "'. Object names can contain only one dot to separate the mod name from the object name.");
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
