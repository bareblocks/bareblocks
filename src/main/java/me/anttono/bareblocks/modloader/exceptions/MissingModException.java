package me.anttono.bareblocks.modloader.exceptions;

import me.anttono.bareblocks.modloader.data.DataSource;

public class MissingModException extends ModLoaderException {
    private final String modName;

    public MissingModException(DataSource source, String modName) {
        super(source, "Trying to access data from missing mod '" + modName + "'.");
        this.modName = modName;
    }

    public String getModName() {
        return modName;
    }
}
