package me.anttono.bareblocks.modloader.exceptions;

import me.anttono.bareblocks.modloader.data.DataSource;
import me.anttono.bareblocks.templates.Mod;

public class ModLoaderException extends Exception {
    private final DataSource source;
    private final String errorInfo;

    public ModLoaderException(DataSource source, String errorInfo) {
        super("Error while loading mod '" + source.getSourceMod().getName() + "' file '" + source.getSourceFile() + "'.\n"
        + "[" + source.getSourcePath() + "]: " + errorInfo);
        this.source = source;
        this.errorInfo = errorInfo;
    }

    public Mod getSourceMod() {
        return source.getSourceMod();
    }

    public String getSourceFile() {
        return source.getSourceFile();
    }

    public String getSourcePath() {
        return source.getSourcePath();
    }

    public String getErrorInfo() {
        return errorInfo;
    }
}
