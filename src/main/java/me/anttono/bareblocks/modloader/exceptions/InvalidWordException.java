package me.anttono.bareblocks.modloader.exceptions;

import me.anttono.bareblocks.modloader.data.DataSource;

public class InvalidWordException extends ModLoaderException {
    private final String variableName;

    public InvalidWordException(DataSource source, String variableName) {
        super(source, "Variable '" + variableName + "' is not allowed to contain whitespaces.");
        this.variableName = variableName;
    }

    public String getVariableName() {
        return variableName;
    }
}
