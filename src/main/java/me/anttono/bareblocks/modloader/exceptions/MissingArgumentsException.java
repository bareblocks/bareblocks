package me.anttono.bareblocks.modloader.exceptions;

import me.anttono.bareblocks.modloader.data.DataSource;

public class MissingArgumentsException extends ModLoaderException {
    private final String functionName;
    private final int requiredCount;
    private final int foundCount;

    public MissingArgumentsException(DataSource source, String functionName, int requiredCount, int foundCount) {
        super(source, "Missing arguments from function '" + functionName + "'. Expected to have " + requiredCount + " arguments but found " + foundCount + ".");
        this.functionName = functionName;
        this.requiredCount = requiredCount;
        this.foundCount = foundCount;
    }

    public String getFunctionName() {
        return functionName;
    }

    public int getRequiredCount() {
        return requiredCount;
    }

    public int getFoundCount() {
        return foundCount;
    }
}
