package me.anttono.bareblocks.modloader;

import me.anttono.bareblocks.api.IntGenerator2D;
import me.anttono.bareblocks.modloader.exceptions.ModLoaderException;
import me.anttono.bareblocks.modloader.data.BlockData;
import me.anttono.bareblocks.modloader.data.FunctionData;
import me.anttono.bareblocks.modloader.data.LayerData;
import me.anttono.bareblocks.modloader.data.WorldGeneratorData;
import me.anttono.bareblocks.templates.BlockTemplate;
import me.anttono.bareblocks.templates.WorldGeneratorTemplate;
import me.anttono.bareblocks.templates.WorldLayerTemplate;
import me.anttono.bareblocks.utils.ModUtils;

import java.util.*;

public class ModCompiler {
    public static final Map<String, IntGenerator2D.Constructor> intGenerator2DFunctions = new HashMap<>();

    /**
     * Compile the BlockData loaded from yml-file to a BlockTemplate. Converts names, etc. to global format. Note that
     * the texture data is not yet loaded.
     * @param blockData the yml-data object to compile.
     * @return the compiled BlockTemplate
     * @throws ModLoaderException if any of the blockData content is invalid.
     */
    public static BlockTemplate compileBlock(BlockData blockData) throws ModLoaderException {
        // Verify fields
        ModUtils.requireWordFieldExistence(blockData.getName(), "name", blockData);
        ModUtils.requireFieldExistence(blockData.getTexture(), "texture", blockData);
        // Construct the template
        String fullName = ModUtils.globalName(blockData.getName(), blockData);
        return new BlockTemplate(fullName, blockData.getTexture());
    }

    /**
     * Compile the WorldGeneratorData loaded from yml-file to a WorldGeneratorTemplate. Converts names, etc. to global
     * format.
     * @param worldGeneratorData the yml-data to compile.
     * @param blockIds map of loaded block ids by their global name.
     * @return the compiled WorldGeneratorTemplate
     * @throws ModLoaderException if any of the worldGeneratorData content is invalid.
     */
    public static WorldGeneratorTemplate compileWorldGenerator(WorldGeneratorData worldGeneratorData, Map<String, Integer> blockIds) throws ModLoaderException {
        // Verify fields
        ModUtils.requireWordFieldExistence(worldGeneratorData.getName(), "name", worldGeneratorData);
        // Construct the template
        String fullName = ModUtils.globalName(worldGeneratorData.getName(), worldGeneratorData);
        List<WorldLayerTemplate> layers = new ArrayList<>();
        for (LayerData layerData : worldGeneratorData.getLayers()) {
            layers.add(compileWorldLayer(layerData, blockIds));
        }
        // Mods define the layers the highest first, but WorldGenerator uses them the lowest first, so the order must be reversed here
        Collections.reverse(layers);
        return new WorldGeneratorTemplate(fullName, layers);
    }

    /**
     * Compile the LayerData loaded from yml-file to a WorldLayerTemplate. Converts names, etc. to global format.
     * @param layerData the yml-data to compile.
     * @param blockIds map of loaded block ids by their global name.
     * @return the compiled worldLayerTemplate.
     * @throws ModLoaderException if any of the layerData content is invalid.
     */
    public static WorldLayerTemplate compileWorldLayer(LayerData layerData, Map<String, Integer> blockIds) throws ModLoaderException {
        // Verify fields
        ModUtils.requireWordFieldExistence(layerData.getName(), "name", layerData);
        ModUtils.requireFieldExistence(layerData.getTopHeightFunction(), "topHeightFunction", layerData);
        ModUtils.requireWordFieldExistence(layerData.getDefaultBlock(), "defaultBlock", layerData);
        ModUtils.requireWordField(layerData.getTopBlock(), "topBlock", layerData);
        // Construct the template
        String layerName = ModUtils.globalName(layerData.getName(), layerData);
        IntGenerator2D topHeightGenerator = compileIntGenerator2D(layerData.getTopHeightFunction());
        int defaultBlockId = blockIds.get(ModUtils.globalName(layerData.getDefaultBlock(), layerData));
        int topBlockId = layerData.hasTopBlock()
                ? blockIds.get(ModUtils.globalName(layerData.getTopBlock(), layerData))
                : defaultBlockId;
        return new WorldLayerTemplate(layerName, topHeightGenerator, defaultBlockId, topBlockId);
    }

    /**
     * Compile the FunctionData loaded from yml-file to a IntGenerator2D. The name of the function should match one of
     * the functions stored in ModCompiler.intGenerator2DFunctions.
     * @param function the yml-data to compile.
     * @return the compiled IntGenerator2D
     * @throws ModLoaderException if any of the function content is invalid.
     */
    public static IntGenerator2D compileIntGenerator2D(FunctionData function) throws ModLoaderException {
        // Verify fields
        ModUtils.requireWordFieldExistence(function.getName(), "name", function);
        // Construct the IntGenerator
        return intGenerator2DFunctions.get(function.getName()).fromFunction(function);
    }

}
