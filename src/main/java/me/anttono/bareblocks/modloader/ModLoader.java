package me.anttono.bareblocks.modloader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import me.anttono.bareblocks.api.ConstantGenerator;
import me.anttono.bareblocks.api.RandomGenerator2D;
import me.anttono.bareblocks.modloader.exceptions.ModLoaderException;
import me.anttono.bareblocks.modloader.data.BlockData;
import me.anttono.bareblocks.modloader.data.ModData;
import me.anttono.bareblocks.modloader.data.WorldGeneratorData;
import me.anttono.bareblocks.templates.BlockTemplate;
import me.anttono.bareblocks.templates.Mod;
import me.anttono.bareblocks.templates.textures.BlockTexture;
import me.anttono.bareblocks.utils.ErrorLogger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class ModLoader {
    private static final String MOD_FOLDER_NAME = "mods";

    // Error logger for errors occurring while loading mods
    private final ErrorLogger logger;

    public ModLoader() {
        this.logger = new ErrorLogger("ModLoader");
    }

    /**
     * Start the main loading process
     * @throws IOException        if the file access fails for any reason
     * @throws ModLoaderException if the mod loading process fails for any reason
     */
    public ModContent load() throws IOException, ModLoaderException {
        // Preload the mods
        verifyFolder(MOD_FOLDER_NAME);
        Mod[] mods = preloadMods();

        // Load the content from the mods (excluding visual content)
        ModData completeModData = new ModData();
        // Step 1: Load all jar files for the mods
        for (Mod mod : mods) {
            loadModJar(mod);
        }
        // Step 2: Load all yaml-files from the mods
        for (Mod mod : mods) {
            for (File file : collectFiles(new File(mod.getScriptFolderPath()))) {
                ObjectMapper mapper = new YAMLMapper();
                ModData modData = mapper.readValue(file, ModData.class);
                modData.setSourceData(mod, file.getPath());
                completeModData.join(modData);
            }
        }
        // Step 3: convert the loaded data into actual templates
        ModContent modContent = new ModContent(mods);
        // Step 3.0: Integrate builtin content
        modContent.addBlockTemplate(new BlockTemplate("air", (BlockTexture) null));
        ModCompiler.intGenerator2DFunctions.put("constant", ConstantGenerator::fromFunction);
        ModCompiler.intGenerator2DFunctions.put("random", RandomGenerator2D::fromFunction);
        // Step 3.1: blocks
        for (BlockData blockData : completeModData.getBlocks()) {
            modContent.addBlockTemplate(ModCompiler.compileBlock(blockData));
        }
        modContent.loadBlockIds();
        // Step 3.2: world generators
        for (WorldGeneratorData worldGeneratorData : completeModData.getWorldGenerators()) {
            modContent.addWorldGeneratorTemplate(
                    ModCompiler.compileWorldGenerator(worldGeneratorData, modContent.getBlockIds())
            );
        }

        System.out.println("Mod loading finished.");
        return modContent;
    }

    /**
     * Check if folder with given path exists. If not, it will be created
     * @param pathName the file path beginning from current working directory as a string
     */
    private void verifyFolder(String pathName) throws IOException {
        // Create path and file object with the given pathName
        Path path = Paths.get(pathName);
        File file = new File(pathName);

        if (!(Files.exists(path) && Files.isDirectory(path))) {
            if (file.mkdirs()) {
                logger.addLine("Directory \"" + pathName + "\" did not exist so it was created.");
            } else {
                throw new IOException("Failed to create a required directory at '" + pathName + "'.");
            }
        }
    }

    /**
     * Collect all valid mods from sub-folders of the mods folder
     * @return array of valid mods found
     * @throws IOException        if the file access fails for any reason
     * @throws ModLoaderException if the mod loading process fails for any reason
     */
    private Mod[] preloadMods() throws IOException, ModLoaderException {
        ArrayList<Mod> mods = new ArrayList<>();

        // Loop through the content of the mods folder
        File dir = new File(MOD_FOLDER_NAME);
        File[] dirContent = dir.listFiles();
        if (dirContent == null) {
            throw new IOException("Failed to read the content of folder '" + dir.getPath() + "'.");
        }
        for (File modFolder : dirContent) {
            Path modPath = modFolder.toPath();
            if (!Files.exists(modPath))
                logger.addWarning("File '" + modPath + "' did not exist.");
            if (!Files.isDirectory(modPath))
                logger.addWarning("File '" + modPath + "' was not a directory, and was ignored.");
            // The directory is considered a valid mod if it has a file called "mod.yml"
            Path modMainFilePath = Paths.get(modPath + "\\mod.yml");
            if (!Files.exists(modMainFilePath)) {
                logger.addWarning("Folder '" + modMainFilePath + "' is not a valid mod because it did not contain mod.yml");
                continue;
            }
            if (Files.isDirectory(modMainFilePath)) {
                logger.addWarning("Folder '" + modMainFilePath + "' is not a valid mod because mod.yml is a directory");
                continue;
            }
            mods.add(preloadMod(modPath.toString()));
        }

        Collections.sort(mods);
        return mods.toArray(new Mod[0]);
    }

    /**
     * Parse the main file of a mod (mod.yml)
     * @param modPath string path to the folder of this mod
     * @return mod object generated by parsing the file
     * @throws IOException        if the file access fails for any reason
     * @throws ModLoaderException if the mod loading process fails for any reason
     */
    private Mod preloadMod(String modPath) throws IOException, ModLoaderException {
        File modFile = new File(modPath + "\\mod.yml");
        ObjectMapper mapper = new YAMLMapper();
        Mod mod = mapper.readValue(modFile, Mod.class);
        mod.setPath(modPath);
        return mod;
    }

    private void loadModJar(Mod mod) {
        // Jar mods are not yet implemented
    }

    /**
     * Recursively find all .yml-files from the given folder and all of its sub-folders and collect them into a single list.
     * @param folder folder to search
     * @return list of yml-files found
     * @throws IOException if the file reading fails for any reason
     */
    private List<File> collectFiles(File folder) throws IOException {
        List<File> files = new ArrayList<>();

        File[] folderContent = folder.listFiles();
        if (folderContent == null) {
            throw new IOException("Failed to read the content of folder '" + folder.getPath() + "'.");
        }
        for (File file : folderContent) {
            Path filePath = file.toPath();
            if (!Files.exists(filePath))
                throw new IOException("File '" + filePath + "' was listed as a file in the parent folder, but didn't actually exist.");
            // Search all sub-folders recursively
            if (Files.isDirectory(filePath)) {
                files.addAll(collectFiles(file));
            }
            // Only collect .yml-files
            String[] scriptFilePathParts = file.toString().split("\\.");
            if (Objects.equals(scriptFilePathParts[scriptFilePathParts.length - 1], "yml")) files.add(file);
        }

        return files;
    }
}
