package me.anttono.bareblocks.modloader.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.anttono.bareblocks.templates.Mod;

public class BlockData extends DataSource {
    @JsonProperty
    private String name;
    @JsonProperty
    private TextureData texture;

    public BlockData() {}

    public void setSourceData(Mod sourceMod, String sourceFile, String sourcePath) {
        super.setSourceData(sourceMod, sourceFile, sourcePath);
        if (texture != null) texture.setSourceData(sourceMod, sourceFile, sourcePath + ".texture");
    }

    public String getName() {
        return name;
    }

    public TextureData getTexture() {
        return texture;
    }
}
