package me.anttono.bareblocks.modloader.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TextureData extends DataSource {
    @JsonProperty
    private String common;
    @JsonProperty
    private String top;
    @JsonProperty
    private String bottom;
    @JsonProperty
    private String north;
    @JsonProperty
    private String east;
    @JsonProperty
    private String south;
    @JsonProperty
    private String west;

    public TextureData() {}

    public String getCommon() {
        return common;
    }

    public String getTop() {
        return top;
    }

    public String getBottom() {
        return bottom;
    }

    public String getNorth() {
        return north;
    }

    public String getEast() {
        return east;
    }

    public String getSouth() {
        return south;
    }

    public String getWest() {
        return west;
    }
}
