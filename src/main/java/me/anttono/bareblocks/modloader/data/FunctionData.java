package me.anttono.bareblocks.modloader.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class FunctionData extends DataSource {
    @JsonProperty
    private String name;
    @JsonProperty
    private List<Integer> arguments;

    public FunctionData() {
        arguments = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<Integer> getArguments() {
        return arguments;
    }
}
