package me.anttono.bareblocks.modloader.data;

import me.anttono.bareblocks.templates.Mod;

public class DataSource {
    private Mod sourceMod;
    private String sourceFile;
    private String sourcePath;

    public void setSourceData(Mod sourceMod, String sourceFile, String sourcePath) {
        this.sourceMod = sourceMod;
        this.sourceFile = sourceFile;
        this.sourcePath = sourcePath;
    }

    public Mod getSourceMod() {
        return sourceMod;
    }

    public String getSourceFile() {
        return sourceFile;
    }

    public String getSourcePath() {
        return sourcePath;
    }
}
