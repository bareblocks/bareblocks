package me.anttono.bareblocks.modloader.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.anttono.bareblocks.templates.Mod;

public class LayerData extends DataSource {
    @JsonProperty
    private String name;
    @JsonProperty
    private FunctionData topHeightFunction;
    @JsonProperty
    private String defaultBlock;
    @JsonProperty
    private String topBlock;

    public LayerData() {}

    public void setSourceData(Mod sourceMod, String sourceFile, String sourcePath) {
        super.setSourceData(sourceMod, sourceFile, sourcePath);
        if (topHeightFunction != null)
            topHeightFunction.setSourceData(sourceMod, sourceFile, sourcePath + ".topHeightFunction(\"" + topHeightFunction.getName() + "\")");
    }

    public String getName() {
        return name;
    }

    public FunctionData getTopHeightFunction() {
        return topHeightFunction;
    }

    public String getDefaultBlock() {
        return defaultBlock;
    }

    public String getTopBlock() {
        return topBlock;
    }

    public boolean hasTopBlock() {
        return this.topBlock != null;
    }
}
