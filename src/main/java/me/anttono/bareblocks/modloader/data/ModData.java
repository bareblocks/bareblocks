package me.anttono.bareblocks.modloader.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.anttono.bareblocks.templates.Mod;

import java.util.ArrayList;
import java.util.List;

public class ModData {
    @JsonProperty
    private List<BlockData> blocks;
    @JsonProperty
    private List<WorldGeneratorData> worldGenerators;

    public ModData() {
        this.blocks = new ArrayList<>();
        this.worldGenerators = new ArrayList<>();
    }

    public void join(ModData other) {
        blocks.addAll(other.getBlocks());
        worldGenerators.addAll(other.getWorldGenerators());
    }

    public void setSourceData(Mod sourceMod, String sourceFile) {
        for (BlockData blockData : this.blocks) {
            blockData.setSourceData(sourceMod, sourceFile, "blocks(\"" + blockData.getName() + "\")");
        }
        for (WorldGeneratorData worldGeneratorData : worldGenerators) {
            worldGeneratorData.setSourceData(sourceMod, sourceFile, "worldGenerators(\"" + worldGeneratorData.getName() + "\")");
        }
    }

    public List<BlockData> getBlocks() {
        return blocks;
    }

    public List<WorldGeneratorData> getWorldGenerators() {
        return worldGenerators;
    }
}
