package me.anttono.bareblocks.modloader.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.anttono.bareblocks.templates.Mod;

import java.util.List;

public class WorldGeneratorData extends DataSource {
    @JsonProperty
    private String name;
    @JsonProperty
    private List<LayerData> layers;

    public WorldGeneratorData() {}

    public void setSourceData(Mod sourceMod, String sourceFile, String sourcePath) {
        super.setSourceData(sourceMod, sourceFile, sourcePath);
        for (LayerData layerData : layers) {
            layerData.setSourceData(sourceMod, sourceFile, sourcePath + ".layers(\"" + layerData.getName() + "\")");
        }
    }

    public String getName() {
        return name;
    }

    public List<LayerData> getLayers() {
        return layers;
    }
}
