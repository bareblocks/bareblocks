package me.anttono.bareblocks.modloader;

import me.anttono.bareblocks.templates.BlockTemplate;
import me.anttono.bareblocks.templates.Mod;
import me.anttono.bareblocks.templates.WorldGeneratorTemplate;

import java.util.*;

public class ModContent {
    private final Mod[] mods;
    private final List<BlockTemplate> blocks;
    private final Map<String, Integer> blockIds;
    private final List<WorldGeneratorTemplate> worldGenerators;

    public ModContent(Mod[] mods) {
        this.mods = mods;
        this.blocks = new ArrayList<>();
        this.blockIds = new HashMap<>();
        this.worldGenerators = new ArrayList<>();
    }

    /**
     * Add all templates from given ModData to this ModData.
     * @param data The data to join to this ModData.
     */
    public void join(ModContent data) {
        addBlockTemplates(data.getBlocks());
        addWorldGeneratorTemplates(data.getWorldGenerators());
    }

    /**
     * Sort the stored data to make the format predictable
     */
    public void sort() {
        Collections.sort(blocks);
    }

    /**
     * Add the given BlockTemplate to the stored data
     * @param blockTemplate the BlockTemplate to store
     */
    public void addBlockTemplate(BlockTemplate blockTemplate) {
        blocks.add(blockTemplate);
    }

    /**
     * Add the given list of blockTemplates to the stored data
     * @param blockTemplates the BlockTemplates to store
     */
    public void addBlockTemplates(List<BlockTemplate> blockTemplates) {
        for (BlockTemplate blockTemplate : blockTemplates) {
            addBlockTemplate(blockTemplate);
        }
    }

    /**
     * Construct a map of all block ids. The ids will be given to all blocks in alphabetical order. After the ids have been loaded, they can be searched with getBlockId()
     */
    public void loadBlockIds() {
        blockIds.clear();
        Collections.sort(blocks);
        for (int i = 0; i < blocks.size(); i++) {
            blockIds.put(blocks.get(i).getName(), i);
        }
    }

    /**
     * A getter for BlockTemplates
     * @return List of all stored BlockTemplates
     */
    public List<BlockTemplate> getBlocks() {
        return blocks;
    }

    /**
     * Get the block id of a block with the given global name.
     * @param blockName a global name of the block to find. A global name contains both the mod name and the block name
     * @return the global id of the block
     */
    public int getBlockId(String blockName) {
        return blockIds.get(blockName);
    }

    /**
     * A getter for map of block ids. Each key is a name of a block with the corresponding id as the value.
     * @return map of block ids.
     */
    public Map<String, Integer> getBlockIds() {
        return blockIds;
    }

    /**
     * Add the given WorldGeneratorTemplate to the stored data
     * @param worldGeneratorTemplate the WorldGeneratorTemplate to store
     */
    public void addWorldGeneratorTemplate(WorldGeneratorTemplate worldGeneratorTemplate) {
        worldGenerators.add(worldGeneratorTemplate);
    }

    /**
     * Add the given list of WorldGeneratorTemplates to the stored data
     * @param worldGeneratorTemplates the WorldGeneratorTemplates to store
     */
    public void addWorldGeneratorTemplates(List<WorldGeneratorTemplate> worldGeneratorTemplates) {
        this.worldGenerators.addAll(worldGeneratorTemplates);
    }

    /**
     * A getter for WorldGeneratorTemplates
     * @return List of all stored WorldGeneratorTemplates
     */
    public List<WorldGeneratorTemplate> getWorldGenerators() {
        return worldGenerators;
    }

    /**
     * A getter for all the mods included in this ModData
     * @return array of all the mods
     */
    public Mod[] getMods() {
        return mods;
    }
}
