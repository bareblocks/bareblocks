package me.anttono.bareblocks.server;

import me.anttono.bareblocks.api.types.BlockPos;
import me.anttono.bareblocks.api.types.ChunkPos;
import me.anttono.bareblocks.server.world.World;
import me.anttono.bareblocks.templates.WorldGeneratorTemplate;
import me.anttono.bareblocks.utils.Timer;
import me.anttono.bareblocks.utils.threading.EventRequestChat;
import me.anttono.bareblocks.utils.threading.events.BlockSetEvent;
import me.anttono.bareblocks.utils.threading.events.Event;
import me.anttono.bareblocks.utils.threading.events.EventType;
import me.anttono.bareblocks.utils.threading.requests.BlockBreakRequest;
import me.anttono.bareblocks.utils.threading.requests.BlockPlaceRequest;
import me.anttono.bareblocks.utils.threading.requests.ChunkLoadRequest;
import me.anttono.bareblocks.utils.threading.requests.Request;

public class GameServer implements Runnable {
    public static final int TARGET_UPS = 60;

    private final Thread serverThread;

    // For communicating with client/s
    private final EventRequestChat eventRequestChat;

    // Stuff related to server time
    private final Timer timer;
    private int tick;

    // Mod data
    private final WorldGeneratorTemplate[] worldGenerators;

    // The world this server is running
    private World world;

    public GameServer(EventRequestChat chat, WorldGeneratorTemplate[] worldGenerators) {
        this.serverThread = new Thread(this, "SERVER_THREAD");

        this.eventRequestChat = chat;
        this.worldGenerators = worldGenerators;

        this.timer = new Timer();
    }

    public void start() {
        serverThread.start();
    }

    @Override
    public void run() {
        System.out.println("Server is running");

        init();

        gameLoop();
    }

    protected void init() {
        this.timer.init();

        // The world is prepared here
        this.world = new World(eventRequestChat, worldGenerators[0]);

        this.tick = 0;
    }

    protected void gameLoop() {
        // Servers main loop
        // Holds a steady ups defined with TARGET_UPS
        // The rest of the work is done by other methods (and timer)
        float elapsedTime;
        float accumulator = 0f;
        float interval = 1f / TARGET_UPS;

        boolean running = true;
        while (running) {
            elapsedTime = timer.getElapsedTime();
            accumulator += elapsedTime;

            while (accumulator >= interval) {
                running = update();
                accumulator -= interval;
            }
        }

        System.out.println("Server stopped.");
    }

    protected boolean update() {
        // Boolean to control whether the server is going to keep on running after this tick or not
        // This way it is more safe to tell the server to stop anywhere in the code.
        boolean running = true;

        // Count ticks
        tick++;

        // Handle upcoming requests
        Request request = eventRequestChat.getRequest();
        while (request != null) {
            System.out.println("SERVER: received request " + request.getRequestType().name());
            switch (request.getRequestType()) {
                case PING_REQUEST: {
                    eventRequestChat.addEvent(new Event(tick, request.getTick(), EventType.PING_EVENT));
                    break;
                }
                case STOP_REQUEST: {
                    running = false;
                    break;
                }
                case CHUNK_LOAD_REQUEST: {
                    ChunkPos chunkPos = ((ChunkLoadRequest) request).getPos();
                    eventRequestChat.addEvent(world.loadChunk(request.getTick(), tick, chunkPos));
                    break;
                }
                case BLOCK_BREAK_REQUEST: {
                    BlockPos blockPos = ((BlockBreakRequest) request).getPos();
                    world.setBlock(blockPos, 0);
                    eventRequestChat.addEvent(new BlockSetEvent(request.getTick(), tick, blockPos, 0));
                    break;
                }
                case BLOCK_PLACE_REQUEST: {
                    BlockPlaceRequest blockPlaceRequest = (BlockPlaceRequest) request;
                    int blockType = blockPlaceRequest.getBlockType();
                    BlockPos pos = blockPlaceRequest.getPos();
                    world.setBlock(pos, blockType);
                    eventRequestChat.addEvent(new BlockSetEvent(request.getTick(), tick, pos, blockType));
                    break;
                }
            }
            request = eventRequestChat.getRequest();
        }

        return running;
    }
}
