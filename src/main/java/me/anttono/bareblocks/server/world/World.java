package me.anttono.bareblocks.server.world;

import me.anttono.bareblocks.api.types.BlockPos;
import me.anttono.bareblocks.api.types.ChunkPos;
import me.anttono.bareblocks.templates.WorldGeneratorTemplate;
import me.anttono.bareblocks.utils.threading.EventRequestChat;
import me.anttono.bareblocks.utils.threading.events.ChunkLoadEvent;
import me.anttono.bareblocks.utils.threading.events.EventType;

import java.util.HashMap;
import java.util.Map;

public class World {
    // The world itself communicates with the client to send chunk data
    // This is the same chat instance as in GameServer.java
    private final EventRequestChat eventRequestChat;

    // The world generator for this world
    private final WorldGenerator worldGenerator;

    // An x-y-z-map of chunks
    private final Map<Integer, Map<Integer, Map<Integer, Chunk>>> chunks;

    public World(EventRequestChat chat, WorldGeneratorTemplate worldGeneratorTemplate) {
        this.eventRequestChat = chat;

        this.worldGenerator = new WorldGenerator(0, worldGeneratorTemplate);

        this.chunks = new HashMap<>();
    }

    public ChunkLoadEvent loadChunk(int requestTick, int currentTick, ChunkPos pos) {
        // Go through the given x, y and z coordinates to make sure that the requested chunk exists
        // If not, generate it using WorldGenerator
        chunks.computeIfAbsent(pos.x, k -> new HashMap<>());
        chunks.get(pos.x).computeIfAbsent(pos.y, k -> new HashMap<>());
        chunks.get(pos.x).get(pos.y).computeIfAbsent(pos.z, k -> worldGenerator.generate(pos));

        return new ChunkLoadEvent(currentTick, requestTick, EventType.CHUNK_LOAD_EVENT, pos, chunks.get(pos.x).get(pos.y).get(pos.z).getData());
    }

    public void setBlock(BlockPos pos, int blockType) {
        ChunkPos chunkPos = pos.toChunkPos();
        ChunkPos.InternalBlockPos blockPos = chunkPos.getInternalBlockPos(pos);
        chunks.get(chunkPos.x).get(chunkPos.y).get(chunkPos.z).setBlock(blockPos, blockType);
    }
}
