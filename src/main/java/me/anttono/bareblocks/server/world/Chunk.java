package me.anttono.bareblocks.server.world;

import me.anttono.bareblocks.api.types.ChunkPos;

public class Chunk {
    private final int[][][] chunkData = new int[16][16][16];

    public void setBlock(int x, int y, int z, int type) {
        this.chunkData[x][y][z] = type;
    }

    public void setBlock(ChunkPos.InternalBlockPos pos, int type) {
        setBlock(pos.x, pos.y, pos.z, type);
    }

    public int getBlock(int x, int y, int z) {
        return this.chunkData[x][y][z];
    }

    public int[][][] getData() {
        return chunkData;
    }
}
