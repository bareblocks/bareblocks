package me.anttono.bareblocks.server.world;

import me.anttono.bareblocks.api.types.PylonPos;
import me.anttono.bareblocks.templates.WorldGeneratorTemplate;
import me.anttono.bareblocks.templates.WorldLayerTemplate;

import java.util.List;

public class Pylon {
    // The main purpose of this class is to store the height layers of one infinitely high chunk sized region.

    private final PylonPos pos;

    private final List<WorldLayerTemplate> layerTemplates;

    private final int[][][] layerTopHeightMap;

    public Pylon(PylonPos pos, WorldGeneratorTemplate generatorTemplate) {
        this.pos = pos;
        this.layerTemplates = generatorTemplate.getLayers();
        this.layerTopHeightMap = new int[16][16][generatorTemplate.getLayerCount()];

        // Generate the height layers for this vertical chunk
        for (int x = 0; x < 16; x++) {
            int globalX = this.pos.x * 16 + x;
            for (int z = 0; z < 16; z++) {
                int globalZ = this.pos.z * 16 + z;
                int layerNumber = 0;
                for (WorldLayerTemplate layer : generatorTemplate.getLayers()) {
                    layerTopHeightMap[x][z][layerNumber] = layer.generateHeight(globalX, globalZ);
                    layerNumber++;
                }
            }
        }
    }

    public Chunk generateChunk(int chunkY) {
        Chunk chunk = new Chunk();

        for (int x = 0; x < 16; x++)
            for (int z = 0; z < 16; z++) {
                int layerNumber = 0;
                for (int y = 0; y < 16; y++) {
                    int worldY = chunkY * 16 + y;

                    // Find out the lowest layer at this position
                    while (layerNumber < layerTemplates.size() && layerTopHeightMap[x][z][layerNumber] < worldY)
                        layerNumber++;

                    // Select the block at this position based on the layer
                    if (layerNumber >= layerTemplates.size()) {
                        // Position is above the highest layer: default block = air
                        chunk.setBlock(x, y, z, 0);
                    } else if (layerTopHeightMap[x][z][layerNumber] == worldY) {
                        chunk.setBlock(x, y, z, layerTemplates.get(layerNumber).getTopBlock());
                    } else {
                        chunk.setBlock(x, y, z, layerTemplates.get(layerNumber).getDefaultBlock());
                    }
                }
            }

        return chunk;
    }
}
