package me.anttono.bareblocks.api;

import me.anttono.bareblocks.modloader.exceptions.ModLoaderException;
import me.anttono.bareblocks.modloader.data.FunctionData;

public interface IntGenerator2D {
    /**
     * Initializes the generator. Deletes any earlier saved world-related data and selects a new seed from the given array.
     * @param seeds a 64-element array of sub-seeds for the world. The generator selects one of them to use
     */
    void init(int[] seeds);

    /**
     * Generates an integer height value for a given 2D-position based on the generator's logic. The generated value should always be the same for the same position.
     * @param x x-coordinate of the height to generate
     * @param z z-coordinate of the height to generate
     * @return integer height value for the position
     */
    int generate(int x, int z);

    interface Constructor {
        IntGenerator2D fromFunction(FunctionData function) throws ModLoaderException;
    }
}
