package me.anttono.bareblocks.api;

import me.anttono.bareblocks.modloader.exceptions.MissingArgumentsException;
import me.anttono.bareblocks.modloader.data.FunctionData;
import me.anttono.bareblocks.utils.ModUtils;

import java.util.List;
import java.util.Random;

public class RandomGenerator2D implements IntGenerator2D {
    private final int seedIndex;
    private int seed;
    private final int minValue;
    private final int range;

    private int chunkX;
    private int chunkZ;
    private final int[][] chunk;

    public RandomGenerator2D(int seedIndex, int minValue, int maxValue) {
        this.seedIndex = seedIndex;
        this.minValue = minValue;
        this.range = maxValue - minValue + 1;
        this.chunk = new int[16][16];
    }

    public static RandomGenerator2D fromFunction(FunctionData function) throws MissingArgumentsException {
        List<Integer> args = function.getArguments();
        ModUtils.requireArgumentCount(args, 3, function.getName(), function);

        int seedIndex = args.get(0);
        int minValue = args.get(1);
        int maxValue = args.get(2);
        return new RandomGenerator2D(seedIndex, minValue, maxValue);
    }

    /**
     * Reset the state of this generator and set a new seed.
     * @param seeds a 64-element array of sub-seeds for the world. The generator selects one of them to use
     */
    @Override
    public void init(int[] seeds) {
        this.seed = seeds[seedIndex];
        generateChunk(0, 0);
    }

    /**
     * Generate all the heights for the given chunk
     * @param x x-coordinate of the chunk
     * @param z z-coordinate of the chunk
     */
    private void generateChunk(int x, int z) {
        chunkX = x;
        chunkZ = z;
        long chunkSeed = seed + x + 1539L * z + (long) x * z;
        Random random = new Random(chunkSeed);
        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < 16; j++) {
                chunk[i][j] = minValue + random.nextInt(range);
            }
        }
    }

    /**
     * Generate a random height value based on the x- and z-coordinate.
     * The values are random from the range of this generator, but deterministic for a position.
     * The function generates the entire chunk of the requested block and saves it to a buffer overwriting the previously buffered chunk.
     * This means that it is optimal to request values from the same chunk in a row before moving to another chunk.
     * @param x x-coordinate of the height to generate
     * @param z z-coordinate of the height to generate
     * @return randomly generated height value for this position
     */
    @Override
    public int generate(int x, int z) {
        int thisChunkX = (int) Math.floor(x / 16f);
        int thisChunkZ = (int) Math.floor(z / 16f);
        if (thisChunkX != chunkX || thisChunkZ != chunkZ) {
            generateChunk(thisChunkX, thisChunkZ);
        }
        int gridX = x % 16;
        if (gridX < 0) gridX += 16;
        int gridZ = z % 16;
        if (gridZ < 0) gridZ += 16;
        return chunk[gridX][gridZ];
    }
}
