package me.anttono.bareblocks.api.types;

public enum Direction {
    NORTH,      // Positive x-axis
    EAST,       // Positive z-axis
    SOUTH,      // Negative x-axis
    WEST,       // Negative z-axis
    UP,         // Positive y-axis
    DOWN,       // Negative y-axis
}
