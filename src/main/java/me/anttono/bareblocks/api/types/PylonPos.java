package me.anttono.bareblocks.api.types;

/**
 * Represents the position of an infinitely high tower of chunks in the world.
 * The position has x and z values with integer values.
 * The position points to the south-western corner of the pylon.
 */
public class PylonPos {
    public int x;
    public int z;

    public PylonPos() {
        this.x = 0;
        this.z = 0;
    }

    public PylonPos(int x, int z) {
        this.x = x;
        this.z = z;
    }

    public boolean equals(PylonPos o) {
        return (this.x == o.x) && (this.z == o.z);
    }
}
