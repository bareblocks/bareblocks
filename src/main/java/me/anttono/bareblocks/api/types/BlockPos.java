package me.anttono.bareblocks.api.types;

import org.joml.Vector3f;

/**
 * Represents position of a block in the world.
 * The position has x, y and z values with integer values.
 * The position points to the lower south-western corner of the block.
 */
public class BlockPos {
    public int x;
    public int y;
    public int z;

    public BlockPos() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    public BlockPos(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public boolean equals(BlockPos o) {
        return (this.x == o.x) && (this.y == o.y) && (this.z == o.z);
    }

    /**
     * Get the next BlockPos in the given direction
     * @param direction
     * @return the next BlockPos
     */
    public BlockPos toward(Direction direction) {
        switch (direction) {
            case NORTH:
                return new BlockPos(x + 1, y, z);
            case SOUTH:
                return new BlockPos(x - 1, y, z);
            case UP:
                return new BlockPos(x, y + 1, z);
            case DOWN:
                return new BlockPos(x, y - 1, z);
            case EAST:
                return new BlockPos(x, y, z + 1);
            case WEST:
                return new BlockPos(x, y, z - 1);
            default:
                // Should never happen
                return this;
        }
    }

    public Vector3f toVector() {
        return new Vector3f(x, y, z);
    }


    /**
     * Converts this position to the position of the chunk in which this position is.
     * In reality this means that the coordinates are divided by 16 and rounded down to the nearest integer.
     * Creates a new ChunkPos object for the position of the chunk.
     * This BlockPos object isn't modified in the process.
     * @return the created ChunkPos object
     */
    public ChunkPos toChunkPos() {
        return new ChunkPos(Math.floorDiv(x, 16), Math.floorDiv(y, 16), Math.floorDiv(z, 16));
    }

    /**
     * Converts this position to the position of the pillar in which this position is.
     * In reality this means that the y coordinate will be dropped and x and z coordinates used to create a PillarPos object.
     * This BlockPos object isn't modified in the process.
     * @return the created PillarPos object
     */
    public PillarPos toPillarPos() {
        return new PillarPos(x, z);
    }

    /**
     * Converts this position to the position of the pylon this position is in.
     * In reality this means that the y coordinate is dropped and x and z coordinates are divided by 16 and rounded down to the nearest integer.
     * Creates a new PylonPos object for the position of the pylon.
     * This BlockPos object isn't modified in the process.
     * @return the created PylonPos object
     */
    public PylonPos toPylonPos() {
        return new PylonPos(Math.floorDiv(x, 16), Math.floorDiv(z, 16));
    }
}
