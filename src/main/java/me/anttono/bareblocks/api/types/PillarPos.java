package me.anttono.bareblocks.api.types;

/**
 * Represents a position of an infinitely high tower of blocks in the world.
 * The position has x and z values with integer values.
 * The position points to the south-western corner of the pillar.
 */
public class PillarPos {
    public int x;
    public int z;

    public PillarPos() {
        this.x = 0;
        this.z = 0;
    }

    public PillarPos(int x, int z) {
        this.x = x;
        this.z = z;
    }

    public boolean equals(PillarPos o) {
        return (this.x == o.x) && (this.z == o.z);
    }

    /**
     * Converts this position to the position of the pylon this position is in.
     * In reality this means that the coordinates are divided by 16 and rounded down to the nearest integer.
     * Creates a new PylonPos object for the position of the pylon.
     * This PillarPos object isn't modified in the process.
     * @return the created PylonPos object
     */
    public PylonPos toPylonPos() {
        return new PylonPos(Math.floorDiv(x, 16), Math.floorDiv(z, 16));
    }
}
