package me.anttono.bareblocks.api.types;

/**
 * Represents a point position anywhere in the world.
 * The position has x, y and z coordinates with floating point values.
 */
public class WorldPos {
    public float x;
    public float y;
    public float z;

    public WorldPos() {
        this.x = 0f;
        this.y = 0f;
        this.z = 0f;
    }

    public WorldPos(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public boolean equals(WorldPos o) {
        return (this.x == o.x) && (this.y == o.y) && (this.z == o.z);
    }

    /**
     * Converts this position to the position of the block in which this position is.
     * In reality this means that the coordinates are rounded down to nearest integer.
     * Creates a new BlockPos object for the position of the block.
     * This WorldPos object isn't modified in the process.
     * @return the created BlockPos object
     */
    public BlockPos toBlockPos() {
        return new BlockPos((int) Math.floor(x), (int) Math.floor(y), (int) Math.floor(z));
    }

    /**
     * Converts this position to the position of the chunk in which this position is.
     * In reality this means that the coordinates are divided by 16f and rounded down to the nearest integer.
     * Creates a new ChunkPos object for the position of the block.
     * This WorldPos object isn't modified in the process.
     * @return the created ChunkPos object
     */
    public ChunkPos toChunkPos() {
        return new ChunkPos((int) Math.floor(x / 16), (int) Math.floor(y / 16), (int) Math.floor(z / 16));
    }

    /**
     * Converts this position to the position of the pillar in which this position is.
     * In reality this means that the y coordinate is dropped and x and z coordinates are rounded down to the nearest integer.
     * Creates a new PillarPos object for the position of the block.
     * This WorldPos object isn't modified in the process.
     * @return the created PillarPos object
     */
    public PillarPos toPillarPos() {
        return new PillarPos((int) Math.floor(x), (int) Math.floor(z));
    }

    /**
     * Converts this position to the position of the pylon this position is in.
     * In reality this means that the y coordinate is dropped and x and z coordinates are divided by 16f and rounded down to the nearest integer.
     * Creates a new PylonPos object for the position of the pylon.
     * This BlockPos object isn't modified in the process.
     * @return the created PylonPos object
     */
    public PylonPos toPylonPos() {
        return new PylonPos((int) Math.floor(x / 16), (int) Math.floor(z / 16));
    }
}
