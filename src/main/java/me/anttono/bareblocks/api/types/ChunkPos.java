package me.anttono.bareblocks.api.types;

import org.joml.Vector3f;

/**
 * Represents position of a chunk in the world.
 * The position has x, y and z values with integer values.
 * The position points to the lower south-western corner of the chunk.
 */
public class ChunkPos {
    public int x;
    public int y;
    public int z;

    public ChunkPos() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    public ChunkPos(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public boolean equals(ChunkPos o) {
        return (this.x == o.x) && (this.y == o.y) && (this.z == o.z);
    }

    public InternalBlockPos getInternalBlockPos(BlockPos pos) {
        return new InternalBlockPos(pos.x - 16 * x, pos.y - 16 * y, pos.z - 16 * z);
    }

    public BlockPos getGlobalBlockPos(InternalBlockPos pos) {
        return new BlockPos(x * 16 + pos.x, y * 16 + pos.y, z * 16 + pos.z);
    }

    /**
     * Check if the given block position is contained in this chunk position
     * @param pos position to check
     * @return true if the position is contained in this chunk position, false otherwise
     */
    public boolean containsPos(BlockPos pos) {
        int xDiff = pos.x - x * 16;
        int yDiff = pos.y - y * 16;
        int zDiff = pos.z - z * 16;
        return (xDiff >= 0 && xDiff < 16) && (yDiff >= 0 && yDiff < 16) && (zDiff >= 0 && zDiff < 16);
    }

    /**
     * Converts this position to the position of the pylon this position is in.
     * In reality this means that the y coordinate will be dropped and x and z coordinates used to create a PylonPos object.
     * This ChunkPos object isn't modified in the process.
     * @return the created PylonPos object
     */
    public PylonPos toPylonPos() {
        return new PylonPos(x, z);
    }

    public Vector3f toVector() {
        return new Vector3f(x * 16, y * 16, z * 16);
    }

    /**
     * Represents position of a block inside a parent chunk.
     * The position has x, y and z values with integer values in range from 0 to 15.
     * The position points to the lower south-western corner of the block.
     */
    public static class InternalBlockPos {
        public int x;
        public int y;
        public int z;

        public InternalBlockPos() {
            this.x = 0;
            this.y = 0;
            this.z = 0;
        }

        public InternalBlockPos(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }
}
