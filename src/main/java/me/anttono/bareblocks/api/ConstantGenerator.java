package me.anttono.bareblocks.api;

import me.anttono.bareblocks.modloader.exceptions.MissingArgumentsException;
import me.anttono.bareblocks.modloader.data.FunctionData;
import me.anttono.bareblocks.utils.ModUtils;

public class ConstantGenerator implements IntGenerator2D {
    private final int value;

    public ConstantGenerator(int value) {
        this.value = value;
    }

    public static ConstantGenerator fromFunction(FunctionData function) throws MissingArgumentsException {
        ModUtils.requireArgumentCount(function.getArguments(), 1, function.getName(), function);

        int value = function.getArguments().get(0);
        return new ConstantGenerator(value);
    }

    /**
     * This function does nothing because the generated constant value is independent of seed.
     * @param seeds unused
     */
    @Override
    public void init(int[] seeds) {
    }

    /**
     * Generate the height value for the given position. Always returns the same constant value of this generator.
     * @param x x-coordinate of the height to generate
     * @param z z-coordinate of the height to generate
     * @return the constant value of this generator
     */
    @Override
    public int generate(int x, int z) {
        return value;
    }
}
